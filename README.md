# sebek-petr

Point-cloud augmentation. Semestral project, bachelor thesis 2020/2021. Personal repository for Petr Šebek

At the moment, https://gitlab.fel.cvut.cz/students/pcl-augmentation is the main repository.

# Contend
* Neural network architecture (code/neural-network)
* Uninformed augmentations (code/augmentations/uninformed)
* Informed augmentations (code/augmentations/informed)

# Input data
All our augmentations are supposed to be used on preprocesed Argoverse dataset. Functions expect that each point cloud has its own .npz file, which contains:
* _pcl_ - point cloud in numpy array with dimension n x 5, where n stands for number of points in point cloud. Columns represents: x, y, z, intensity, and the label of each point.
* _anno_ - annotations of objects in point cloud in the form of dictionary. Each annotation contains: x, y, z coordinates of the center of the bounding box, rotation of the bounding box (in quaternions), bounding box lenght, width, and height, its track ID, and class label.
* _transfor_matrix_ - numpy array with dimension 4x4, which is projection to the global coordination system.
* _anno_info_ - dictionary, which codes original Argoverse labes to ours.

Our code also expects that each sequence has its own directory and that these directories are also in another directory for example, training_data/sequence_name/point_cloud.npz

Our code also needs to have calibration data in the form of json file. Json files need to be named vehicle_calibration_info.json and they need to be stored similar to npz files (training_data/sequence_name/vehicle_calibration_info.json)

# Neural network
Our neural network is based on U-net architecture and for their implementation we use PyTorch.

# Uninformed augmentations
_make_field_of_view.py_ creates FoV. In this script are boolen variables with names of uninformed augmentations, if some are set to True script apply this augmentation to input data.

_uninformed_augmentation.py_ stores definition of functions, which _make_field_of_view.py_ uses.

# Informed augmentations
## Insertion
Main function is _insertion.py_ before this function can be executed, Road map and bounding box point clouds need to be created.

### Road map
For Insertion Road map must be created by following steps:
1. Execute _single_drivable_area_map.py_
2. Execute _large_drivable_area_map.py_
3. Execute _pedestrian-area.py_ (mandatory just for pedestrian insertion)

### Separating bounding box point clouds
Function _cut_bbox.py_ creates .npz files with bounding box point clouds, which is used for insertion. Execute this function before main function (_insertion.py_).

## Movement simulation
Main function is _movement_simulation.py_ before this function can be executed, Composed stationary scenes and list of moving objects need to be created.

### List of moving objects
A list of moving objects must be executed first by executing _find_moving_objects.py_.

### Composed stationary scenes
Composed stationary scenes are created by function _make_composed_stationary_scene.py_.

Note: our programs cannot create directories.
