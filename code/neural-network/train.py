import os
import numpy as np
import torch
import torch.nn as nn
import torch.utils.data as tdata
import time
import sys

from torch.utils.data import DataLoader

from neural_network import *

EPOCH_NUMBER = 100
LEARNING_RATE = 0.001
BS = 16
FROM_SCRATCH = True

TRAIN_DATA_PATH = ''    # address of directory with training data
VAL_DATA_PATH = ''      # address of directory with validation data
SAVE_PATH = ''          # address of directory for saving models

TRANSLATION = False
ROTATION = False
MIRROR_REFLECTION = False
RANDOM_NOISE = False
RANDOM_POINT_REMOVAL = False
VEHICLE_INSERTION = False
BIKES_INSERTION = False
PEDESTRIAN_INSERTION = False
MOVEMENT_SIMULATION = False

# weights in cross entropy loss
W0 = 0.00018261  # Background
W1 = 0.00115293  # Vehicle
W2 = 0.04276512  # Pedestrian
W3 = 0.75977998  # Bikes
W4 = 0.19435562  # Road blocks
W5 = 0.00170901  # Road
W6 = 0.00005469  # Not arrived


class Dataset(tdata.Dataset):
    def __init__(self, folder_list):
        super().__init__()
        self.data_folders = folder_list
        self.files = np.array([os.listdir(self.data_folders[0])])
        print('Not augmented:', len(self.files[0]), self.data_folders[0])
        for i in range(1, len(self.data_folders)):
            self.files = np.concatenate((self.files, [os.listdir(folder_list[i])]), axis=0)
            print('Augmentation', i, len(self.files[i]), self.data_folders[i])

    def __len__(self):
        length = 0
        for i in range(0, len(self.files)):
            length += len(self.files[i])
        # print('Length of dataset is:', length)
        return length

    def __getitem__(self, i):
        for k in range(0, len(self.files)):
            if i >= len(self.files[k]):
                i -= len(self.files[k])
            else:
                self.data_npz = np.load(self.data_folders[k] + '/' + self.files[k][i], allow_pickle=True)
                break

        self.data = self.data_npz['train']
        self.labels = self.data_npz['label']
        return {
            'labels': np.asarray(self.labels).astype('i8'),
            # torch wants labels to be of type LongTensor, in order to compute losses
            'data': np.asarray(self.data).astype('f4').transpose((2, 0, 1)),
            'key': i,  # for saving of the data
        }


def get_free_gpu():
    os.system('nvidia-smi -q -d Memory |grep -A4 GPU|grep Free >tmp')
    finish = False
    temp = 0
    index = 0
    while not finish:
        time.sleep(0.2)
        memory_available = [int(x.split()[2]) for x in open('tmp', 'r').readlines()]
        index = int(np.argmax(memory_available))  # Skip the 7th card --- it is reserved for evaluation!!!
        print(memory_available[index], temp, "\r", end="")
        temp += 1
        temp = temp % 10
        if memory_available[index] >= 11100:
            finish = True
    return index


def get_device(gpu):  # Manually specify gpu
    if torch.cuda.is_available():
        device = torch.device(gpu)
        print("GPU:", gpu, 'is now mine!!!!!')
    else:
        device = 'cpu'
        print("CPU")

    return device


def accuracy(prediction, labels_batch, dim=1):
    pred_index = prediction.argmax(dim)
    return (pred_index == labels_batch).float().mean()


if __name__ == '__main__':
    # MOVE TO GPU
    device = get_device(int(sys.argv[1]))
    torch.cuda.empty_cache()
    # device = 'cpu'
    start_run = int(sys.argv[2])
    start_epoch = int(sys.argv[3])
    acc_recovery = float(sys.argv[4])
    if acc_recovery >= 1:
        print('Unrealistic ACC')
        exit(10)

    print('Start run is', start_run, 'and start epoch is', start_epoch, 'ACC recovery', acc_recovery)

    for r in range(start_run, start_run+1):
        # MODEL INITIALIZATION
        if r != start_run:
            start_epoch = 0

        if FROM_SCRATCH and (not (r == start_run and start_epoch != 0)):
            model = MySegmentation_deep()
            print('Straring from scratch')
        else:
            model = load_model(SAVE_PATH + str(start_run) + '/last_state.pth')
            print('Model loaded:', SAVE_PATH + str(start_run) + '/last_state.pth')

        train_data = np.array([TRAIN_DATA_PATH + 'baseline/data'])
        if TRANSLATION:
            train_data = np.append(train_data, [TRAIN_DATA_PATH + 'translation/data'])
        if ROTATION:
            train_data = np.append(train_data, [TRAIN_DATA_PATH + 'rotation/data'])
        if MIRROR_REFLECTION:
            train_data = np.append(train_data, [TRAIN_DATA_PATH + 'mirror_reflection/data'])
        if RANDOM_NOISE:
            train_data = np.append(train_data, [TRAIN_DATA_PATH + 'random_noise/data'])
        if RANDOM_POINT_REMOVAL:
            train_data = np.append(train_data, [TRAIN_DATA_PATH + 'random_point_removal/data'])
        if VEHICLE_INSERTION:
            train_data = np.append(train_data, [TRAIN_DATA_PATH + 'vehicle_insertion/data'])
        if BIKES_INSERTION:
            train_data = np.append(train_data, [TRAIN_DATA_PATH + 'bikes_insertion/data'])
        if PEDESTRIAN_INSERTION:
            train_data = np.append(train_data, [TRAIN_DATA_PATH + 'pedestrian_insertion/data'])
        if MOVEMENT_SIMULATION:
            train_data = np.append(train_data, [TRAIN_DATA_PATH + 'movement_simulation/data'])

        validation_data = np.array([VAL_DATA_PATH])

        # LOAD DATA
        print("Loading data...")
        trn_dataset = Dataset(train_data)

        print("Train data loaded")
        val_dataset = Dataset(validation_data)

        print("Val data loaded")
        ###

        trn_loader = DataLoader(trn_dataset, batch_size=BS, num_workers=4, shuffle=True)
        val_loader = DataLoader(val_dataset, batch_size=BS, num_workers=4)

        model = model.to(device)

        # UPDATE FUNCTION
        # criterion = DiceLoss.DiceLoss().to(device)
        criterion = nn.CrossEntropyLoss(weight=torch.tensor((W0, W1, W2, W3, W4, W5, W6), dtype=torch.float)).to(device)

        optimizer = torch.optim.Adam(params=model.parameters(), lr=LEARNING_RATE)
        ###
        if start_epoch != 0:
            best_acc = acc_recovery
        else:
            best_acc = 0

        train_loss_list = []
        val_loss_list = []
        acc_list = []

        # TRAINING LOOP
        for epoch in range(start_epoch, EPOCH_NUMBER):
            model.train()

            loss_list = 0
            loss_val = 0
            acc = 0
            it = 0

            # TRAIN
            print("Start training")
            for it, batch in enumerate(trn_loader):
                input_data = batch['data']
                labels = batch['labels']
                input_data = input_data.to(device).requires_grad_()
                labels = labels.to(device)

                output = model(input_data)

                loss = criterion(output, labels)
                loss.backward()
                optimizer.step()
                optimizer.zero_grad()
                loss_list += loss.item()

                del input_data
                del labels
                del batch
                del output
                del loss

            # VALIDATION
            print("Start validation")
            with torch.no_grad():
                model.eval()

                for val_it, batch in enumerate(val_loader):
                    input_data = batch['data']
                    labels = batch['labels']
                    input_data = input_data.to(device)
                    labels = labels.to(device)

                    output = model(input_data)
                    loss = criterion(output, labels)

                    loss_val += loss.item()

                    tmp = accuracy(output, labels)
                    acc += tmp.item()

                    del input_data
                    del labels
                    del batch
                    del output
                    del loss
                    del tmp

                # ACCURACY ON VALIDATION DATASET
                acc = acc/(val_it+1)
                print('SAVING MODEL')
                save_model(model, SAVE_PATH + str(r) + '/last_state.pth')

                # Saving best model
                if acc > best_acc:
                    best_acc = acc
                    save_model(model, SAVE_PATH + str(r) + '/' + f'acc{int(acc * 100):03d}.pth')
                    print("New best accuracy. Model saved.")

                print(f'\nEpoch: {epoch+1:03d} \t Trn Loss: {loss_list / (it + 1):.3f} \t Val Loss: {loss_val / (val_it + 1):.3f} \t Acc: {acc * 100 :.1f}%')
                train_loss_list.append(loss_list / (it + 1))
                val_loss_list.append(loss_val / (val_it + 1))
                acc_list.append(acc)

        print('TRAIN LOSS:', train_loss_list)
        print('VAL LOSS:', val_loss_list)
        print('ACC:', acc_list)
