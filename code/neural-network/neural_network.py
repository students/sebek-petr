import torch
import torch.nn as nn

INPUT_CHANNELS = 3
OUTPUT_CHANNELS = 7

L1_CHANNELS = 32
L2_CHANNELS = 64
L3_CHANNELS = 128
L4_CHANNELS = 256
L5_CHANNELS = 512


class MySegmentation_deep(nn.Module):
    def __init__(self):
        super().__init__()

        self.active_function = torch.relu
        self.soft_max = nn.Softmax(dim=1)
        self.max_pool = nn.MaxPool2d(kernel_size=2, stride=2, return_indices=True)
        self.max_unpool = nn.MaxUnpool2d(kernel_size=2, stride=2)

        self.batch_norm_1_1 = nn.BatchNorm2d(num_features=L1_CHANNELS, eps=1e-5, momentum=0.1, affine=True,
                                           track_running_stats=True)
        self.batch_norm_1_2 = nn.BatchNorm2d(num_features=L1_CHANNELS, eps=1e-5, momentum=0.1, affine=True,
                                             track_running_stats=True)
        self.batch_norm_1_3 = nn.BatchNorm2d(num_features=L1_CHANNELS, eps=1e-5, momentum=0.1, affine=True,
                                             track_running_stats=True)
        self.batch_norm_1_4 = nn.BatchNorm2d(num_features=L1_CHANNELS, eps=1e-5, momentum=0.1, affine=True,
                                             track_running_stats=True)
        self.batch_norm_1_5 = nn.BatchNorm2d(num_features=L1_CHANNELS, eps=1e-5, momentum=0.1, affine=True,
                                             track_running_stats=True)
        self.batch_norm_2_1 = nn.BatchNorm2d(num_features=L2_CHANNELS, eps=1e-5, momentum=0.1, affine=True,
                                           track_running_stats=True)
        self.batch_norm_2_2 = nn.BatchNorm2d(num_features=L2_CHANNELS, eps=1e-5, momentum=0.1, affine=True,
                                           track_running_stats=True)
        self.batch_norm_2_3 = nn.BatchNorm2d(num_features=L2_CHANNELS, eps=1e-5, momentum=0.1, affine=True,
                                           track_running_stats=True)
        self.batch_norm_2_4 = nn.BatchNorm2d(num_features=L2_CHANNELS, eps=1e-5, momentum=0.1, affine=True,
                                           track_running_stats=True)
        self.batch_norm_3_1 = nn.BatchNorm2d(num_features=L3_CHANNELS, eps=1e-5, momentum=0.1, affine=True,
                                           track_running_stats=True)
        self.batch_norm_3_2 = nn.BatchNorm2d(num_features=L3_CHANNELS, eps=1e-5, momentum=0.1, affine=True,
                                           track_running_stats=True)
        self.batch_norm_3_3 = nn.BatchNorm2d(num_features=L3_CHANNELS, eps=1e-5, momentum=0.1, affine=True,
                                           track_running_stats=True)
        self.batch_norm_3_4 = nn.BatchNorm2d(num_features=L3_CHANNELS, eps=1e-5, momentum=0.1, affine=True,
                                           track_running_stats=True)
        self.batch_norm_4_1 = nn.BatchNorm2d(num_features=L4_CHANNELS, eps=1e-5, momentum=0.1, affine=True,
                                           track_running_stats=True)
        self.batch_norm_4_2 = nn.BatchNorm2d(num_features=L4_CHANNELS, eps=1e-5, momentum=0.1, affine=True,
                                           track_running_stats=True)
        self.batch_norm_4_3 = nn.BatchNorm2d(num_features=L4_CHANNELS, eps=1e-5, momentum=0.1, affine=True,
                                             track_running_stats=True)
        self.batch_norm_4_4 = nn.BatchNorm2d(num_features=L3_CHANNELS, eps=1e-5, momentum=0.1, affine=True,
                                             track_running_stats=True)
        self.batch_norm_5_1 = nn.BatchNorm2d(num_features=L5_CHANNELS, eps=1e-5, momentum=0.1, affine=True,
                                             track_running_stats=True)
        self.batch_norm_5_2 = nn.BatchNorm2d(num_features=L5_CHANNELS, eps=1e-5, momentum=0.1, affine=True,
                                             track_running_stats=True)
        self.batch_norm_5_3 = nn.BatchNorm2d(num_features=L4_CHANNELS, eps=1e-5, momentum=0.1, affine=True,
                                             track_running_stats=True)

        self.conv_down_1_1 = nn.Conv2d(in_channels=INPUT_CHANNELS, out_channels=L1_CHANNELS, kernel_size=7, padding=3)
        self.conv_down_1_2 = nn.Conv2d(in_channels=L1_CHANNELS, out_channels=L1_CHANNELS, kernel_size=7, padding=3)
        self.conv_down_2_1 = nn.Conv2d(in_channels=L1_CHANNELS, out_channels=L2_CHANNELS, kernel_size=5, padding=2)
        self.conv_down_2_2 = nn.Conv2d(in_channels=L2_CHANNELS, out_channels=L2_CHANNELS, kernel_size=3, padding=1)
        self.conv_down_3_1 = nn.Conv2d(in_channels=L2_CHANNELS, out_channels=L3_CHANNELS, kernel_size=3, padding=1)
        self.conv_down_3_2 = nn.Conv2d(in_channels=L3_CHANNELS, out_channels=L3_CHANNELS, kernel_size=3, padding=1)
        self.conv_down_4_1 = nn.Conv2d(in_channels=L3_CHANNELS, out_channels=L4_CHANNELS, kernel_size=3, padding=1)
        self.conv_down_4_2 = nn.Conv2d(in_channels=L4_CHANNELS, out_channels=L4_CHANNELS, kernel_size=3, padding=1)

        self.conv_mid_1 = nn.Conv2d(in_channels=L4_CHANNELS, out_channels=L5_CHANNELS, kernel_size=3, padding=1)
        self.conv_mid_2 = nn.Conv2d(in_channels=L5_CHANNELS, out_channels=L5_CHANNELS, kernel_size=3, padding=1)
        self.conv_mid_3 = nn.Conv2d(in_channels=L5_CHANNELS, out_channels=L4_CHANNELS, kernel_size=3, padding=1)

        self.conv_up_4_1 = nn.Conv2d(in_channels=L4_CHANNELS*2, out_channels=L4_CHANNELS, kernel_size=3, padding=1)
        self.conv_up_4_2 = nn.Conv2d(in_channels=L4_CHANNELS, out_channels=L3_CHANNELS, kernel_size=3, padding=1)
        self.conv_up_3_1 = nn.Conv2d(in_channels=L3_CHANNELS*2, out_channels=L3_CHANNELS, kernel_size=3, padding=1)
        self.conv_up_3_2 = nn.Conv2d(in_channels=L3_CHANNELS, out_channels=L2_CHANNELS, kernel_size=3, padding=1)
        self.conv_up_2_1 = nn.Conv2d(in_channels=L2_CHANNELS*2, out_channels=L2_CHANNELS, kernel_size=3, padding=1)
        self.conv_up_2_2 = nn.Conv2d(in_channels=L2_CHANNELS, out_channels=L1_CHANNELS, kernel_size=3, padding=1)
        self.conv_up_1_1 = nn.Conv2d(in_channels=L1_CHANNELS*2, out_channels=L1_CHANNELS, kernel_size=3, padding=1)
        self.conv_up_1_2 = nn.Conv2d(in_channels=L1_CHANNELS, out_channels=L1_CHANNELS, kernel_size=3, padding=1)

        self.conv_out = nn.Conv2d(in_channels=L1_CHANNELS, out_channels=OUTPUT_CHANNELS, kernel_size=3, padding=1)

        self.weight_init()

    def forward(self, x):

        # DOWN
        # first layer 3 to L1
        x = self.conv_down_1_1(x)
        x = self.batch_norm_1_1(x)
        x = self.active_function(x)

        x_skip1 = self.conv_down_1_2(x)
        x = self.batch_norm_1_2(x_skip1)
        x = self.active_function(x)

        x, indicate_1 = self.max_pool(x)


        # second layer L1 to L2
        x = self.conv_down_2_1(x)
        x = self.batch_norm_2_1(x)
        x = self.active_function(x)

        x_skip2 = self.conv_down_2_2(x)
        x = self.batch_norm_2_2(x_skip2)
        x = self.active_function(x)

        x, indicate_2 = self.max_pool(x)


        # third layer L2 to L3
        x = self.conv_down_3_1(x)
        x = self.batch_norm_3_1(x)
        x = self.active_function(x)

        x_skip3 = self.conv_down_3_2(x)
        x = self.batch_norm_3_2(x_skip3)
        x = self.active_function(x)

        x, indicate_3 = self.max_pool(x)

        # fourth layer L3 to L4
        x = self.conv_down_4_1(x)
        x = self.batch_norm_4_1(x)
        x = self.active_function(x)

        x_skip4 = self.conv_down_4_2(x)
        x = self.batch_norm_4_2(x_skip4)
        x = self.active_function(x)

        x, indicate_4 = self.max_pool(x)


        # MIDDLE L4 to L5 to L4
        x = self.conv_mid_1(x)
        x = self.batch_norm_5_1(x)
        x = self.active_function(x)

        x = self.conv_mid_2(x)
        x = self.batch_norm_5_2(x)
        x = self.active_function(x)

        x = self.conv_mid_3(x)
        x = self.batch_norm_5_3(x)
        x = self.active_function(x)


        # UP
        # fourth layer L4
        x = self.max_unpool(x, indicate_4)
        x = torch.cat([x, x_skip4], 1)

        x = self.conv_up_4_1(x)
        x = self.batch_norm_4_3(x)
        x = self.active_function(x)

        x = self.conv_up_4_2(x)
        x = self.batch_norm_4_4(x)
        x = self.active_function(x)


        # third layer L3
        x = self.max_unpool(x, indicate_3)
        x = torch.cat([x, x_skip3], 1)

        x = self.conv_up_3_1(x)
        x = self.batch_norm_3_4(x)
        x = self.active_function(x)

        x = self.conv_up_3_2(x)
        x = self.batch_norm_2_3(x)
        x = self.active_function(x)


        # second layer L3 to L2
        x = self.max_unpool(x, indicate_2)
        x = torch.cat([x, x_skip2], 1)

        x = self.conv_up_2_1(x)
        x = self.batch_norm_2_4(x)
        x = self.active_function(x)

        x = self.conv_up_2_2(x)
        x = self.batch_norm_1_3(x)
        x = self.active_function(x)


        # first layer L2 to L1
        x = self.max_unpool(x, indicate_1)
        x = torch.cat([x, x_skip1], 1)

        x = self.conv_up_1_1(x)
        x = self.batch_norm_1_4(x)
        x = self.active_function(x)

        x = self.conv_up_1_2(x)
        x = self.batch_norm_1_5(x)
        x = self.active_function(x)


        # OUT
        x = self.conv_out(x)
        x = self.soft_max(x)
        return x

    def weight_init(self):
        for lay in self.modules():
            if type(lay) in [torch.nn.Conv2d, torch.nn.Linear]:
                torch.nn.init.xavier_uniform_(lay.weight)


def save_model(model, destination):
    torch.save(model.state_dict(), destination)


def load_model(destination):
    model = MySegmentation_deep()
    model.load_state_dict(torch.load(destination, map_location='cpu'))
    model.eval()
    return model
