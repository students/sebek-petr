import random
import copy
import numpy as np
import math

NUMCOLUMN = 360 * 4
NUMROW = 112

X_TRANSLATION = 10
Y_TRANSLATION = 0

AZIMUT_ROTATION = 1.4

NOISE_RANGE_DISTANCE = 0.03     # (-0.03;0.03)
NOISE_RANGE_INTENSITY = 3       # (-3;3)
DROP_OUT_RATE = 0.05            # probability of drop out pixel


def mirror_reflection(train, label):
    c1 = random.randint(0, NUMCOLUMN)

    for c in range(1, int((NUMCOLUMN/2))):

        left_column = c1-c
        if left_column < 0:
            left_column = NUMCOLUMN + left_column

        rigth_column = c1+c
        if rigth_column > NUMCOLUMN-1:
            rigth_column = rigth_column % NUMCOLUMN

        tmp_train = copy.deepcopy(train[:, left_column])
        tmp_label = copy.deepcopy(label[:, left_column])
        train[:, left_column] = train[:, rigth_column]
        label[:, left_column] = label[:, rigth_column]
        train[:, rigth_column] = tmp_train
        label[:, rigth_column] = tmp_label

    return train, label


def fill_spherical_translation(pc, max_beta, min_beta):         # X, Y, Z, r, alpha, beta, Intensity, Label
    pc[:, 3] = np.sqrt(pc[:, 0] ** 2 + pc[:, 1] ** 2 + pc[:, 2] ** 2)  # r (0;infty)
    pc[:, 4] = np.arctan2(pc[:, 1], pc[:, 0]) + np.pi  # alpha (0;2pi)
    pc[:, 5] = np.arccos(pc[:, 2] / pc[:, 3])  # beta

    pc = pc[pc[:, 5] <= max_beta]
    pc = pc[pc[:, 5] >= min_beta]

    return pc


def translation(point_cloud, max_beta, min_beta):
    x_move = np.random.uniform(-0.1, 0.1) + 0.1414 * np.sign(np.random.uniform(-1, 1))
    y_move = np.random.uniform(-0.1, 0.1) + 0.1414 * np.sign(np.random.uniform(-1, 1))
    point_cloud[:, 0] = point_cloud[:, 0] - x_move
    point_cloud[:, 1] = point_cloud[:, 1] - y_move

    point_cloud = fill_spherical_translation(point_cloud, max_beta, min_beta)

    return point_cloud


def rotation(point_cloud):
    azimut_move = np.random.uniform(0, 2 * math.pi)
    point_cloud[:, 4] += azimut_move
    point_cloud[:, 4] = point_cloud[:, 4] % (2*math.pi)

    return point_cloud


def random_noise(train, label):
    for r in range(NUMROW):
        for c in range(NUMCOLUMN):
            if label[r][c] != 6:
                train[r][c][0] += np.random.uniform(-NOISE_RANGE_DISTANCE, NOISE_RANGE_DISTANCE)
                train[r][c][1] += np.random.uniform(-NOISE_RANGE_INTENSITY, NOISE_RANGE_INTENSITY)
    return train


def random_point_removal(train, label):
    for r in range(NUMROW):
        for c in range(NUMCOLUMN):
            if np.random.uniform(0, 1) < DROP_OUT_RATE:
                train[r][c][0] = 0
                train[r][c][1] = 0
                train[r][c][2] = 0

                label[r][c] = 6
    return train, label