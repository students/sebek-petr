import numpy as np
import os
import json
import math
from PIL import Image

from uninformed_augumentation import *

CROP = True

MAKE = 'train'
# MAKE = 'validation'
# MAKE = 'test'

CROP_X = 100
CROP_Y = 50
CROP_Z = 10

TRANSLATION = False
ROTATION = False
MIRROR_REFLECTION = False
RANDOM_NOISE = False
RANDOM_POINT_REMOVAL = False

RGB_CLASS = np.array([[255, 0, 0], [255, 255, 0], [255, 0, 255], [0, 0, 255], [123, 123, 123], [0, 255, 0], [0, 0, 0]])

DATA_PATH = ''      # address of directory with point clouds data
JSON_PATH = ''      # address of directory with json

TRAIN_DIRECTORY = ['train1', 'train2', 'train3', 'train4']
TRAIN_SEQUENCES = [['f3fb839e-0aa2-342b-81c3-312b80be44f9', 'b3def699-884b-3c9e-87e1-1ab76c618e0b', '11953248-1195-1195-1195-511954366464', '26d141ec-f952-3908-b4cc-ae359377424e', '10b8dee6-778f-33e4-a946-d842d2d9c3d7', 'c6911883-1843-3727-8eaa-41dc8cda8993', '230970eb-dc2e-3133-b252-ff3c6f5d4284', '8a15674a-ae5c-38e2-bc4b-f4156d384072', '88538208-8853-8853-8853-388539396096', '64c12551-adb9-36e3-a0c1-e43a0e9f3845', 'dcdcd8b3-0ba1-3218-b2ea-7bb965aad3f0', 'e17eed4f-3ffd-3532-ab89-41a3f24cf226'],
                   ['f0826a9f-f46e-3c27-97af-87a77f7899cd', 'fb471bd6-7c81-3d93-ad12-ac54a28beb84', '53213cf0-540b-3b5a-9900-d24d1d41bda0', 'ebe7a98b-d383-343b-96d6-9e681e2c6a36', '02cf0ce1-699a-373b-86c0-eb6fd5f4697a', 'fa0b626f-03df-35a0-8447-021088814b8b', '38b2c7ef-069b-3d9d-bbeb-8847b8c89fb6', '0ef28d5c-ae34-370b-99e7-6709e1c4b929'],
                   ['aebe6aaa-6a95-39e6-9a8d-06103141fcde', 'ff78e1a3-6deb-34a4-9a1f-b85e34980f06', '08a8b7f0-c317-3bdb-b3dc-b7c9b6d033e2', '919be600-da69-3f09-b0fd-f42f7eb2e097', 'de777454-df62-3d5a-a1ce-2edb5e5d4922', 'd4d9e91f-0f8e-334d-bd0e-0d062467308a', '99c45b6e-6fc7-39b8-80d7-727c485fb561', '10f92308-e06e-3725-a302-4b09e6e790ad', '6c739f57-96d0-33e6-972d-af29cc527e1f', '52af191b-ba56-326c-b569-e37790db40f3'],
                   ['15c802a9-0f0e-3c87-b516-a3fa02f1ecb0', '2bc6a872-9979-3493-82eb-fb55407473c9', '49d66e75-3ce6-316b-b589-f659c7ef5e6d']]

VAL_TEST_DIRECTORY = ['val']
VALIDATION_SEQUENCES = [['1d676737-4110-3f7e-bec0-0c90f74c248f', '2d12da1d-5238-3870-bfbc-b281d5e8c1a1', '5ab2697b-6e3e-3454-a36a-aba2c6f27818', '33737504-3373-3373-3373-633738571776', 'da734d26-8229-383f-b685-8086e58d1e05', 'cb0cba51-dfaf-34e9-a0c2-d931404c3dd8', '033669d3-3d6b-3d3d-bd93-7985d86653ea']]

TEST_SEQUENCES = [['e9a96218-365b-3ecd-a800-ed2c4c306c78', 'cd64733a-dd8a-3bdf-b46a-b7144226168a', '70d2aea5-dbeb-333d-b21e-76a7f2f1ba1c', '7d37fc6b-1028-3f6f-b980-adb5fa73021e', 'f9fa3960-537f-3151-a1a3-37a9c0d6d7f7', '85bc130b-97ae-37fb-a129-4fc07c80cca7', 'f1008c18-e76e-3c24-adcc-da9858fac145']]

SAVE_OUTPUT_FOLDER = ''     # address of directory for saving outputs


def create_image(labels, filename):
    """
    Function, which create Image of classes.
    :param labels: numpy 2D array with classes
    :param filename: string, name of the image
    """

    columns = len(labels[0])
    lines = len(labels)
    rgb = np.zeros(3 * columns * lines).reshape(lines, columns, 3)

    for i in range(lines):
        for j in range(columns):
            labels_idx = int(labels[i][j])
            rgb[i][j] = RGB_CLASS[labels_idx]

    rgb = np.uint8(rgb)
    img = Image.fromarray(rgb, 'RGB')
    img.save(filename)
    return


def add_space_for_spherical(point_cloud):
    points_num = len(point_cloud)
    out = np.ones((points_num, 9))*-6
    out[:, 0:3] = point_cloud[:, 0:3]    # X, Y, Z
    out[:, 6:8] = point_cloud[:, 3:5]    # Intensity, Label
    return out  # X, Y, Z, r, alpha, beta, Intensity, Label


def fill_spherical(point_cloud):         # X, Y, Z, r, alpha, beta, Intensity, Label
    point_cloud[:, 3] = np.sqrt(point_cloud[:, 0] ** 2 + point_cloud[:, 1] ** 2 + point_cloud[:, 2] ** 2)  # r (0;infty)
    point_cloud[:, 4] = np.arctan2(point_cloud[:, 1], point_cloud[:, 0]) + np.pi  # alpha (0;2pi)
    point_cloud[:, 5] = np.arccos(point_cloud[:, 2] / point_cloud[:, 3])  # beta

    min_beta_geometrical = np.min(point_cloud[:, 5])
    max_beta_geometrical = np.max(point_cloud[:, 5])

    return point_cloud, max_beta_geometrical, min_beta_geometrical


def geometrical_front_view(point_cloud, num_row, num_column, max_beta, min_beta): # X, Y, Z, r, alpha, beta, intensity, label

    point_num = len(point_cloud)
    dbeta = (max_beta - min_beta) / num_row
    daplha = 2 * math.pi / num_column
    label = np.ones((num_row, num_column)) * 6
    train = np.zeros((num_row, num_column, 3))  # distance, intensity, Not arrived

    for i in range(point_num):
        point = point_cloud[i]

        row = int((point[5] - min_beta) / dbeta)
        if point[5] == max_beta:
            row = num_row-1

        column = int(point[4] / daplha)

        if label[row][column] != 6:

            point_cloud[i][8] = row * NUMCOLUMN + column

            if train[row][column][0] > point[3]:
                label[row][column] = point[7]  # label

                train[row][column][0] = point[3]  # distance
                train[row][column][1] = point[6]  # intensity
                train[row][column][2] = 1  # arrived

        else:
            label[row][column] = point[7]  # label

            train[row][column][0] = point[3]  # distance
            train[row][column][1] = point[6]  # intensity
            train[row][column][2] = 1  # arrived

            point_cloud[i][8] = row * NUMCOLUMN + column

    return train, label


if __name__ == '__main__':
    if MAKE == 'train':
        directory_list = TRAIN_DIRECTORY
        sequence_list = TRAIN_SEQUENCES
        print('Making train dataset')

    elif MAKE == 'validation':
        directory_list = VAL_TEST_DIRECTORY
        sequence_list = VALIDATION_SEQUENCES
        print('Making validation dataset')
    else:
        directory_list = VAL_TEST_DIRECTORY
        sequence_list = TEST_SEQUENCES
        print('Making test dataset')

    for directory_index in range(len(directory_list)):
        for sequence_index in range(len(sequence_list)):

            current_directory = os.listdir(DATA_PATH + directory_list[directory_index] + '/' + sequence_list[directory_index][sequence_index])
            print(DATA_PATH + directory_list[directory_index] + '/' + sequence_list[directory_index][sequence_index])

            with open(JSON_PATH + directory_list[directory_index] + '/' + sequence_list[directory_index][sequence_index] + '/vehicle_calibration_info.json') as json_data_file:
                lidar_info = json.load(json_data_file)
                lidar_coordinates = (np.array(lidar_info['vehicle_SE3_down_lidar_']['translation']) +
                                     np.array(lidar_info['vehicle_SE3_up_lidar_']['translation'])) / 2

            for file in current_directory:
                if file.endswith('.npz'):
                    print(DATA_PATH + directory_list[directory_index] + '/' + sequence_list[directory_index][sequence_index] + '/' + file)

                    data = np.load(DATA_PATH + directory_list[directory_index] + '/' + sequence_list[directory_index][sequence_index] + '/' + file)

                    point_cloud = data['pcl']

                    road = False

                    for k in range(len(point_cloud)):
                        if point_cloud[k][4] == 5:
                            road = True
                            break

                    if not road:
                        print('ROAD NOT ANNOTATED')
                        continue

                    point_cloud[:, 0:3] = point_cloud[:, 0:3] - lidar_coordinates

                    # CROP DATA
                    if CROP:
                        point_cloud = point_cloud[point_cloud[:, 2] < CROP_Z]
                        point_cloud = point_cloud[point_cloud[:, 0] < CROP_X]
                        point_cloud = point_cloud[point_cloud[:, 0] > -CROP_X]
                        point_cloud = point_cloud[point_cloud[:, 1] < CROP_Y]
                        point_cloud = point_cloud[point_cloud[:, 1] > -CROP_Y]
                    else:
                        point_cloud = point_cloud

                    # CORRECTION OF ROAD
                    for k in range(len(point_cloud)):
                        if point_cloud[k][4] == 5 and point_cloud[k][2] >= 0.3-lidar_coordinates[2]:
                            point_cloud[k][4] = 0

                    point_cloud = add_space_for_spherical(point_cloud)

                    point_cloud, max_beta, min_beta = fill_spherical(point_cloud)


                    train, label = geometrical_front_view(point_cloud, NUMROW, NUMCOLUMN, max_beta, min_beta)

                    png_name = file.split('.')[0]

                    # AUGMENTATION
                    if TRANSLATION:
                        point_cloud = translation(point_cloud, max_beta, min_beta)
                        train, label = geometrical_front_view(point_cloud, NUMROW, NUMCOLUMN, max_beta, min_beta)

                    if ROTATION:
                        point_cloud = rotation(point_cloud)
                        train, label = geometrical_front_view(point_cloud, NUMROW, NUMCOLUMN, max_beta, min_beta)

                    if MIRROR_REFLECTION:
                        train, label = mirror_reflection(train, label)

                    if RANDOM_NOISE:
                        train = random_noise(train, label)

                    if RANDOM_POINT_REMOVAL:
                        train, label = random_point_removal(train, label)

                    # NORMALIZE DATA
                    train[:, :, 0] = train[:, :, 0]/(np.sqrt(CROP_X**2+CROP_Y**2+CROP_Z**2))
                    if np.max(train[:, :, 0]) > 1 or np.max(train[:, :, 1]) > 1:
                        print('NORMALIZATION ERROR')

                    # SAVE IMAGE
                    create_image(label, SAVE_OUTPUT_FOLDER + 'picture/' + png_name + '.png')

                    # SAVE EACH NPZ
                    np.savez(SAVE_OUTPUT_FOLDER + 'data/' + file, train=train, label=label)
