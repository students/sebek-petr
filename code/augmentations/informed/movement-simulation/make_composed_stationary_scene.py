import numpy as np
import os
from scipy.spatial.transform import Rotation as R
from PIL import Image
import copy

RGB_CLASS = np.array([[255, 0, 0], [255, 255, 0], [255, 0, 255], [0, 0, 255], [123, 123, 123], [0, 255, 0], [0, 0, 0], [0, 191, 255]])

DATA_PATH = ''      # address with training data
JSON_PATH = ''      # address with json files

MOVING_PATH = ''    # address with output from find_moving_objects.py

TRAIN_DIRECTORY = ['train1', 'train2', 'train3', 'train4']
TRAIN_SEQUENCES = [['f3fb839e-0aa2-342b-81c3-312b80be44f9', 'b3def699-884b-3c9e-87e1-1ab76c618e0b', '11953248-1195-1195-1195-511954366464', '26d141ec-f952-3908-b4cc-ae359377424e', '10b8dee6-778f-33e4-a946-d842d2d9c3d7', 'c6911883-1843-3727-8eaa-41dc8cda8993', '230970eb-dc2e-3133-b252-ff3c6f5d4284', '8a15674a-ae5c-38e2-bc4b-f4156d384072', '88538208-8853-8853-8853-388539396096', '64c12551-adb9-36e3-a0c1-e43a0e9f3845', 'dcdcd8b3-0ba1-3218-b2ea-7bb965aad3f0', 'e17eed4f-3ffd-3532-ab89-41a3f24cf226'],
                   ['f0826a9f-f46e-3c27-97af-87a77f7899cd', 'fb471bd6-7c81-3d93-ad12-ac54a28beb84', '53213cf0-540b-3b5a-9900-d24d1d41bda0', 'ebe7a98b-d383-343b-96d6-9e681e2c6a36', '02cf0ce1-699a-373b-86c0-eb6fd5f4697a', 'fa0b626f-03df-35a0-8447-021088814b8b', '38b2c7ef-069b-3d9d-bbeb-8847b8c89fb6', '0ef28d5c-ae34-370b-99e7-6709e1c4b929'],
                   ['aebe6aaa-6a95-39e6-9a8d-06103141fcde', 'ff78e1a3-6deb-34a4-9a1f-b85e34980f06', '08a8b7f0-c317-3bdb-b3dc-b7c9b6d033e2', '919be600-da69-3f09-b0fd-f42f7eb2e097', 'de777454-df62-3d5a-a1ce-2edb5e5d4922', 'd4d9e91f-0f8e-334d-bd0e-0d062467308a', '99c45b6e-6fc7-39b8-80d7-727c485fb561', '10f92308-e06e-3725-a302-4b09e6e790ad', '6c739f57-96d0-33e6-972d-af29cc527e1f', '52af191b-ba56-326c-b569-e37790db40f3'],
                   ['15c802a9-0f0e-3c87-b516-a3fa02f1ecb0', '2bc6a872-9979-3493-82eb-fb55407473c9', '49d66e75-3ce6-316b-b589-f659c7ef5e6d']]

SAVE_OUTPUT_FOLDER = ''     # address of directory to save data (composed stationary scenes)


def create_image(labels, filename):
    """
    Function, which create Image of classes.
    :param labels: numpy 2D array with classes
    :param filename: string, name of the image
    """

    columns = len(labels[0])
    lines = len(labels)
    rgb = np.zeros(3 * columns * lines).reshape(lines, columns, 3)

    for i in range(lines):
        for j in range(columns):
            labels_idx = int(labels[i][j])
            rgb[i][j] = RGB_CLASS[labels_idx]

    rgb = np.uint8(rgb)
    img = Image.fromarray(rgb, 'RGB')
    img.save(filename)


def separate_bbox(point_cloud, annotation, annotation_info, annotation_move = [0, 0, 0]):
    xc = annotation['center']['x'] - annotation_move[0]
    yc = annotation['center']['y'] - annotation_move[1]
    zc = annotation['center']['z'] - annotation_move[2]
    Q1 = annotation['rotation']['x']
    Q2 = annotation['rotation']['y']
    Q3 = annotation['rotation']['z']
    Q4 = annotation['rotation']['w']
    length = annotation['length'] + 0.2
    width = annotation['width'] + 0.2
    height = annotation['height'] + 0.2
    label_class = annotation['label_class']
    anno_info = annotation_info.item()
    class_index = anno_info[label_class]

    r = R.from_quat([Q1, Q2, Q3, Q4])
    rot_matrix = r.as_dcm()

    mask_1 = (rot_matrix[0][0] * point_cloud[:, 0] + rot_matrix[1][0] * point_cloud[:, 1] + rot_matrix[2][
            0] * point_cloud[:,
                 2] >
        rot_matrix[0][0] * (xc + rot_matrix[0][0] * length / 2) + rot_matrix[1][0] * (
                yc + rot_matrix[1][0] * length / 2) + rot_matrix[2][0] *
        (zc + rot_matrix[2][0] * length / 2))
    mask_2 = (rot_matrix[0][0] * point_cloud[:, 0] + rot_matrix[1][0] * point_cloud[:, 1] + rot_matrix[2][0] * point_cloud[:, 2] <
              rot_matrix[0][0] * (xc - rot_matrix[0][0] * length / 2) + rot_matrix[1][0] * (
                yc - rot_matrix[1][0] * length / 2) + rot_matrix[2][0] *
              (zc - rot_matrix[2][0] * length / 2))
    mask_3 = (rot_matrix[0][1] * point_cloud[:, 0] + rot_matrix[1][1] * point_cloud[:, 1] + rot_matrix[2][1] * point_cloud[:, 2] >
              rot_matrix[0][1] * (xc + rot_matrix[0][1] * width / 2) + rot_matrix[1][1] * (
                yc + rot_matrix[1][1] * width / 2) + rot_matrix[2][1] *
              (zc + rot_matrix[2][1] * width / 2))
    mask_4 = (rot_matrix[0][1] * point_cloud[:, 0] + rot_matrix[1][1] * point_cloud[:, 1] + rot_matrix[2][1] * point_cloud[:, 2] <
              rot_matrix[0][1] * (xc - rot_matrix[0][1] * width / 2) + rot_matrix[1][1] * (
                yc - rot_matrix[1][1] * width / 2) + rot_matrix[2][1] *
              (zc - rot_matrix[2][1] * width / 2))
    mask_5 = (rot_matrix[0][2] * point_cloud[:, 0] + rot_matrix[1][2] * point_cloud[:, 1] + rot_matrix[2][2] * point_cloud[:, 2] >
              rot_matrix[0][2] * (xc + rot_matrix[0][2] * height / 2) + rot_matrix[1][2] * (
                yc + rot_matrix[1][2] * height / 2) + rot_matrix[2][2] *
              (zc + rot_matrix[2][2] * height / 2))
    mask_6 = (rot_matrix[0][2] * point_cloud[:, 0] + rot_matrix[1][2] * point_cloud[:, 1] + rot_matrix[2][2] * point_cloud[:, 2] <
              rot_matrix[0][2] * (xc - rot_matrix[0][2] * height / 2) + rot_matrix[1][2] * (
                yc - rot_matrix[1][2] * height / 2) + rot_matrix[2][2] *
              (zc - rot_matrix[2][2] * height / 2))
    mask_7 = (point_cloud[:, 4] != class_index)



    # final_mask = mask_1
    final_mask = np.ma.mask_or(mask_1, mask_2)
    final_mask = np.ma.mask_or(final_mask, mask_3)
    final_mask = np.ma.mask_or(final_mask, mask_4)
    final_mask = np.ma.mask_or(final_mask, mask_5)
    final_mask = np.ma.mask_or(final_mask, mask_6)
    final_mask = np.ma.mask_or(final_mask, mask_7)

    scene = point_cloud[final_mask]
    bbox = point_cloud[final_mask == False]

    return scene, bbox


if __name__ == '__main__':

    directory_list = TRAIN_DIRECTORY
    sequence_list = TRAIN_SEQUENCES

    for directory_index in range(len(directory_list)):
        for sequence_index in range(len(sequence_list[directory_index])):

            current_directory = os.listdir(DATA_PATH + directory_list[directory_index] + '/' + sequence_list[directory_index][sequence_index])
            print(DATA_PATH + directory_list[directory_index] + '/' + sequence_list[directory_index][sequence_index])

            current_directory.sort()

            combine_scene = np.array([])
            position = np.zeros((4, 1))
            position[3][0] = 1

            moving_objects_data = np.load(MOVING_PATH + directory_list[directory_index] + '/' + sequence_list[directory_index][sequence_index] + '_moving_objects.npz', allow_pickle=True)
            moving_objects = moving_objects_data['objects']

            for i in range(len(current_directory)):
                file = current_directory[i]
                if file.endswith('.npz'):
                    data = np.load(DATA_PATH + directory_list[directory_index] + '/' + sequence_list[directory_index][sequence_index] + '/' + file, allow_pickle=True)

                    transform_matrix = data['transform_matrix']
                    point_cloud = data['pcl']
                    annotation = data['anno']
                    annotation_info = data['anno_info']

                    for anno in annotation:

                        row = np.where(moving_objects == anno['track_label_uuid'])

                        if len(row[0]) == 0:
                            continue
                        else:
                            point_cloud, trash = separate_bbox(point_cloud, anno, annotation_info)
                            print('Remove object')

                    for p in range(len(point_cloud)):
                        point = point_cloud[p]
                        position[0:3, 0] = point[0:3].T

                        position = np.dot(transform_matrix, position)

                        point_cloud[p, 0:3] = copy.deepcopy(position[0:3, 0].T)

                    if len(combine_scene) == 0:
                        combine_scene = copy.deepcopy(point_cloud)
                    else:
                        combine_scene = np.append(combine_scene, copy.deepcopy(point_cloud), axis=0)

            np.savez(SAVE_OUTPUT_FOLDER + directory_list[directory_index] + '/' + sequence_list[directory_index][sequence_index] + '_combine_scene.npz', pcl=combine_scene)