import numpy as np
from skimage.util import img_as_ubyte
from skimage.morphology import closing
from skimage.morphology import rectangle
from PIL import Image

PIPELINE = [5, 0, 1, 3, 4, 2]
DISK_SIZE = 3
# Road, Background, Car, Bikes, Road blocks, Humans
RGB_CLASS = np.array([[255, 0, 0], [255, 255, 0], [255, 0, 255], [0, 0, 255], [123, 123, 123], [0, 255, 0], [0, 0, 0], [255, 192, 203]])

def create_image(labels, filename):
    """
    Function, which create Image of classes.
    :param labels: numpy 2D array with classes
    :param filename: string, name of the image
    """

    columns = len(labels[0])
    lines = len(labels)
    rgb = np.zeros(3 * columns * lines).reshape(lines, columns, 3)

    for i in range(lines):
        for j in range(columns):
            labels_idx = int(labels[i][j])
            rgb[i][j] = RGB_CLASS[labels_idx]

    rgb = np.uint8(rgb)
    img = Image.fromarray(rgb, 'RGB')
    img.save(filename)
    return


def class_closing(original_label, class_idx):
    class_mask = original_label.copy()

    for row in range(original_label.shape[0]):
        for column in range(original_label.shape[1]):
            if original_label[row][column] != class_idx:
                class_mask[row][column] = 0
            else:
                class_mask[row][column] = 1

    mask_phantom = img_as_ubyte(class_mask)
    selem = rectangle(5, 3)
    closed = closing(mask_phantom, selem)

    return closed


def smooth_out(original_train, original_label):
    train = original_train.copy()
    label = original_label.copy()

    for i in range(len(PIPELINE)):                              # closing per class
        class_index = PIPELINE[i]
        tmp = class_closing(original_label, class_index)
        for row in range(original_label.shape[0]):              # compare closing results
            for column in range(original_label.shape[1]):
                if (tmp[row][column] == 1 and train[row][column] == class_index) or tmp[row][column] == 0:
                    continue
                else:
                    neighbors = 0
                    sum_distance = 0
                    for drow in range(-2, 3):
                        for dcolumn in range(-1, 2):
                            if drow + row > -1 and drow + row < original_label.shape[0] and dcolumn + column > -1 and dcolumn + column < original_label.shape[1] and original_label[drow + row][dcolumn + column] == class_index:
                                neighbors += 1
                                sum_distance += original_train[row + drow][column + dcolumn][0]
                    if neighbors == 0:
                        label[row][column] = class_index
                    else:
                        train[row][column][0] = sum_distance/neighbors
                        label[row][column] = class_index

    return train, label