import numpy as np
import os
from scipy.spatial.transform import Rotation as R
from PIL import Image
import math
import json
import copy
from closing import *

RGB_CLASS = np.array([[255, 0, 0], [255, 255, 0], [255, 0, 255], [0, 0, 255], [123, 123, 123], [0, 255, 0], [0, 0, 0], [0, 191, 255]])

CROP = True

CROP_X = 100
CROP_Y = 50
CROP_Z = 10

NUMCOLUMN = 360 * 4
NUMROW = 112
TIME = 3    # in 0.1s

DATA_PATH = ''      # address of directory with training data
JSON_PATH = ''      # address of directory with json files
TRACKING_PATH = ''  # address of outputs from find_moving_objects.py and make_composed_stationary_scene.py

TRAIN_DIRECTORY = ['train4', 'train3', 'train2', 'train1']
TRAIN_SEQUENCES = [['15c802a9-0f0e-3c87-b516-a3fa02f1ecb0', '2bc6a872-9979-3493-82eb-fb55407473c9', '49d66e75-3ce6-316b-b589-f659c7ef5e6d'],
                   ['aebe6aaa-6a95-39e6-9a8d-06103141fcde', 'ff78e1a3-6deb-34a4-9a1f-b85e34980f06', '08a8b7f0-c317-3bdb-b3dc-b7c9b6d033e2', '919be600-da69-3f09-b0fd-f42f7eb2e097', 'de777454-df62-3d5a-a1ce-2edb5e5d4922', 'd4d9e91f-0f8e-334d-bd0e-0d062467308a', '99c45b6e-6fc7-39b8-80d7-727c485fb561', '10f92308-e06e-3725-a302-4b09e6e790ad', '6c739f57-96d0-33e6-972d-af29cc527e1f', '52af191b-ba56-326c-b569-e37790db40f3'],
                   ['f0826a9f-f46e-3c27-97af-87a77f7899cd', 'fb471bd6-7c81-3d93-ad12-ac54a28beb84', '53213cf0-540b-3b5a-9900-d24d1d41bda0', 'ebe7a98b-d383-343b-96d6-9e681e2c6a36', '02cf0ce1-699a-373b-86c0-eb6fd5f4697a', 'fa0b626f-03df-35a0-8447-021088814b8b', '38b2c7ef-069b-3d9d-bbeb-8847b8c89fb6', '0ef28d5c-ae34-370b-99e7-6709e1c4b929'],
                   ['e17eed4f-3ffd-3532-ab89-41a3f24cf226', 'dcdcd8b3-0ba1-3218-b2ea-7bb965aad3f0', '64c12551-adb9-36e3-a0c1-e43a0e9f3845', '88538208-8853-8853-8853-388539396096', '8a15674a-ae5c-38e2-bc4b-f4156d384072', '230970eb-dc2e-3133-b252-ff3c6f5d4284', 'c6911883-1843-3727-8eaa-41dc8cda8993', '10b8dee6-778f-33e4-a946-d842d2d9c3d7', '26d141ec-f952-3908-b4cc-ae359377424e', '11953248-1195-1195-1195-511954366464', 'b3def699-884b-3c9e-87e1-1ab76c618e0b', 'f3fb839e-0aa2-342b-81c3-312b80be44f9']]

SAVE_OUTPUT_FOLDER = ''     # address of directory for saving augmented data


def create_image(labels, filename):
    """
    Function, which create Image of classes.
    :param labels: numpy 2D array with classes
    :param filename: string, name of the image
    """

    columns = len(labels[0])
    lines = len(labels)
    rgb = np.zeros(3 * columns * lines).reshape(lines, columns, 3)

    for i in range(lines):
        for j in range(columns):
            labels_idx = int(labels[i][j])
            rgb[i][j] = RGB_CLASS[labels_idx]

    rgb = np.uint8(rgb)
    img = Image.fromarray(rgb, 'RGB')
    img.save(filename)


def separate_bbox(point_cloud, annotation, anno_info, annotation_move = [0, 0, 0]):
    bbox = None
    xc = annotation['center']['x'] - annotation_move[0]
    yc = annotation['center']['y'] - annotation_move[1]
    zc = annotation['center']['z'] - annotation_move[2]
    Q1 = annotation['rotation']['x']
    Q2 = annotation['rotation']['y']
    Q3 = annotation['rotation']['z']
    Q4 = annotation['rotation']['w']
    length = annotation['length']
    width = annotation['width']
    height = annotation['height']
    label_class = annotation['label_class']
    anno_info = anno_info.item()
    class_index = anno_info[label_class]

    r = R.from_quat([Q1, Q2, Q3, Q4])
    rot_matrix = r.as_dcm()

    mask_1 = (rot_matrix[0][0] * point_cloud[:, 0] + rot_matrix[1][0] * point_cloud[:, 1] + rot_matrix[2][
            0] * point_cloud[:,
                 2] >
        rot_matrix[0][0] * (xc + rot_matrix[0][0] * length / 2) + rot_matrix[1][0] * (
                yc + rot_matrix[1][0] * length / 2) + rot_matrix[2][0] *
        (zc + rot_matrix[2][0] * length / 2))
    mask_2 = (rot_matrix[0][0] * point_cloud[:, 0] + rot_matrix[1][0] * point_cloud[:, 1] + rot_matrix[2][0] * point_cloud[:, 2] <
              rot_matrix[0][0] * (xc - rot_matrix[0][0] * length / 2) + rot_matrix[1][0] * (
                yc - rot_matrix[1][0] * length / 2) + rot_matrix[2][0] *
              (zc - rot_matrix[2][0] * length / 2))
    mask_3 = (rot_matrix[0][1] * point_cloud[:, 0] + rot_matrix[1][1] * point_cloud[:, 1] + rot_matrix[2][1] * point_cloud[:, 2] >
              rot_matrix[0][1] * (xc + rot_matrix[0][1] * width / 2) + rot_matrix[1][1] * (
                yc + rot_matrix[1][1] * width / 2) + rot_matrix[2][1] *
              (zc + rot_matrix[2][1] * width / 2))
    mask_4 = (rot_matrix[0][1] * point_cloud[:, 0] + rot_matrix[1][1] * point_cloud[:, 1] + rot_matrix[2][1] * point_cloud[:, 2] <
              rot_matrix[0][1] * (xc - rot_matrix[0][1] * width / 2) + rot_matrix[1][1] * (
                yc - rot_matrix[1][1] * width / 2) + rot_matrix[2][1] *
              (zc - rot_matrix[2][1] * width / 2))
    mask_5 = (rot_matrix[0][2] * point_cloud[:, 0] + rot_matrix[1][2] * point_cloud[:, 1] + rot_matrix[2][2] * point_cloud[:, 2] >
              rot_matrix[0][2] * (xc + rot_matrix[0][2] * height / 2) + rot_matrix[1][2] * (
                yc + rot_matrix[1][2] * height / 2) + rot_matrix[2][2] *
              (zc + rot_matrix[2][2] * height / 2))
    mask_6 = (rot_matrix[0][2] * point_cloud[:, 0] + rot_matrix[1][2] * point_cloud[:, 1] + rot_matrix[2][2] * point_cloud[:, 2] <
              rot_matrix[0][2] * (xc - rot_matrix[0][2] * height / 2) + rot_matrix[1][2] * (
                yc - rot_matrix[1][2] * height / 2) + rot_matrix[2][2] *
              (zc - rot_matrix[2][2] * height / 2))
    mask_7 = (point_cloud[:, 7] != class_index)

    # final_mask = mask_1
    final_mask = np.ma.mask_or(mask_1, mask_2)
    final_mask = np.ma.mask_or(final_mask, mask_3)
    final_mask = np.ma.mask_or(final_mask, mask_4)
    final_mask = np.ma.mask_or(final_mask, mask_5)
    final_mask = np.ma.mask_or(final_mask, mask_6)
    final_mask = np.ma.mask_or(final_mask, mask_7)

    scene = point_cloud[final_mask]
    bbox = point_cloud[final_mask == False]

    return scene, bbox


def add_space_for_spherical(point_cloud):
    '''
    :param point_cloud: original point-cloud as 2D array
    :return: point-cloud as 2D array with space for spherical coordination
    '''
    points_num = len(point_cloud)
    out = np.ones((points_num, 9))*-6
    out[:, 0:3] = point_cloud[:, 0:3]    # X, Y, Z
    out[:, 6:8] = point_cloud[:, 3:5]    # Intensity, Label
    return out  # X, Y, Z, r, alpha, beta, Intensity, Label, index


def fill_spherical(point_cloud):         # X, Y, Z, r, alpha, beta, Intensity, Label
    '''
    :param point_cloud: point-cloud as 2D array
    :return: point-cloud as 2D array with computed spherical coordination
    '''
    point_cloud[:, 3] = np.sqrt(point_cloud[:, 0] ** 2 + point_cloud[:, 1] ** 2 + point_cloud[:, 2] ** 2)  # r (0;infty)
    point_cloud[:, 4] = np.arctan2(point_cloud[:, 1], point_cloud[:, 0]) + np.pi  # alpha (0;2pi)
    point_cloud[:, 5] = np.arccos(point_cloud[:, 2] / point_cloud[:, 3])  # beta

    min_beta_geometrical = np.min(point_cloud[:, 5])
    max_beta_geometrical = np.max(point_cloud[:, 5])

    return point_cloud, max_beta_geometrical, min_beta_geometrical


def geometrical_front_view(point_cloud, num_row, num_column, max_beta, min_beta): # X, Y, Z, r, alpha, beta, Intensity, Label
    '''
    :param point_cloud: point-cloud as 2D array
    :param num_row: front view row resolution
    :param num_column: rront view column resolution
    :param max_beta: maximal elevation angle in original point-cloud
    :param min_beta: minimal elevation angle in original point-cloud
    :return: front view and labels for front view
    '''
    point_num = len(point_cloud)
    dbeta = (max_beta-min_beta) / num_row       # resolution in row
    daplha = 2 * math.pi / num_column           # resolution in column
    label = np.ones((num_row, num_column)) * 6
    train = np.zeros((num_row, num_column, 3))  # distance, intensity, arrived?

    for i in range(point_num):
        point = point_cloud[i]

        row = int((point[5] - min_beta) / dbeta)
        if point[5] == max_beta:
            row = num_row - 1

        if point[5] > max_beta:
            continue

        if point[5] < min_beta:
            continue

        column = int(point[4] / daplha)

        if label[row][column] != 6:

            point_cloud[i][8] = row * NUMCOLUMN + column

            if train[row][column][0] > point[3]:
                label[row][column] = point[7]  # label

                train[row][column][0] = point[3]  # distance
                train[row][column][1] = point[6]  # intensity
                train[row][column][2] = 1  # arrived

        else:
            label[row][column] = point[7]     # label

            train[row][column][0] = point[3]     # distance
            train[row][column][1] = point[6]     # intensity
            train[row][column][2] = 1            # arrived

            point_cloud[i][8] = row * NUMCOLUMN + column

    return train, label, point_cloud


def make_move_list(annotation, moving_objects):
    return_annotation = copy.deepcopy(annotation)
    for i in range(len(annotation)-1, -1, -1):
        anno = annotation[i]
        row = np.where(moving_objects == anno['track_label_uuid'])
        if len(row[0]) == 0:
            return_annotation = np.delete(return_annotation, i)
    return return_annotation



if __name__ == '__main__':
    '''
    making driveable area from scenes
    '''
    directory_list = TRAIN_DIRECTORY
    sequence_list = TRAIN_SEQUENCES

    position = np.zeros((4, 1))
    position[3][0] = 1

    for directory_index in range(len(directory_list)):
        for sequence_index in range(len(sequence_list[directory_index])):

            dence_scene = np.load(TRACKING_PATH + 'combine_stationary_scene/' + directory_list[directory_index] + '/' + sequence_list[directory_index][sequence_index] + '_combine_scene.npz', allow_pickle=True)
            dence_scene = dence_scene['pcl']

            moving_objects = np.load(TRACKING_PATH + 'moving_objects/' + directory_list[directory_index] + '/' + sequence_list[directory_index][sequence_index] + '_moving_objects.npz', allow_pickle=True)
            moving_objects = moving_objects['objects']

            dence_scene_backup = copy.deepcopy(dence_scene)

            current_directory = os.listdir(DATA_PATH + directory_list[directory_index] + '/' + sequence_list[directory_index][sequence_index])
            print(DATA_PATH + directory_list[directory_index] + '/' + sequence_list[directory_index][sequence_index])

            with open(JSON_PATH + directory_list[directory_index] + '/' + sequence_list[directory_index][sequence_index] + '/vehicle_calibration_info.json') as json_data_file:
                lidar_info = json.load(json_data_file)
                lidar_coordinates = (np.array(lidar_info['vehicle_SE3_down_lidar_']['translation']) +
                                     np.array(lidar_info['vehicle_SE3_up_lidar_']['translation'])) / 2

            current_directory.sort()

            for f in range(len(current_directory)):
                file = current_directory[f]

                dence_scene = copy.deepcopy(dence_scene_backup)

                if file.endswith('.npz'):
                    print(directory_list[directory_index] + '/' + sequence_list[directory_index][sequence_index] + '/' + file)
                    if os.path.exists(SAVE_OUTPUT_FOLDER + 'data/' + file):
                        print('Already done')
                        continue

                    data = np.load(DATA_PATH + directory_list[directory_index] + '/' + sequence_list[directory_index][sequence_index] + '/' + file, allow_pickle=True)
                    scene_pcl = data['pcl']
                    annotation = data['anno']
                    anno_info = data['anno_info']
                    transform_matrix = data['transform_matrix']
                    inversion_transform_matrix = np.linalg.inv(transform_matrix)

                    # dence scene from global to local
                    for p in range(len(dence_scene)):
                        point = dence_scene[p]
                        position[0:3, 0] = point[0:3].T

                        position = np.dot(inversion_transform_matrix, position)

                        dence_scene[p, 0:3] = copy.deepcopy(position[0:3, 0].T)

                    print('Inverse complete')

                    # LiDAR position correction
                    dence_scene[:, 0:3] = dence_scene[:, 0:3] - lidar_coordinates
                    scene_pcl[:, 0:3] = scene_pcl[:, 0:3] - lidar_coordinates

                    # Crop point clouds
                    if CROP:
                        dence_scene = dence_scene[dence_scene[:, 2] < CROP_Z]
                        dence_scene = dence_scene[dence_scene[:, 0] < CROP_X]
                        dence_scene = dence_scene[dence_scene[:, 0] > -CROP_X]
                        dence_scene = dence_scene[dence_scene[:, 1] < CROP_Y]
                        dence_scene = dence_scene[dence_scene[:, 1] > -CROP_Y]
                    else:
                        dence_scene = dence_scene

                    if CROP:
                        scene_pcl = scene_pcl[scene_pcl[:, 2] < CROP_Z]
                        scene_pcl = scene_pcl[scene_pcl[:, 0] < CROP_X]
                        scene_pcl = scene_pcl[scene_pcl[:, 0] > -CROP_X]
                        scene_pcl = scene_pcl[scene_pcl[:, 1] < CROP_Y]
                        scene_pcl = scene_pcl[scene_pcl[:, 1] > -CROP_Y]
                    else:
                        scene_pcl = scene_pcl

                    # Correct road annotation
                    for k in range(len(scene_pcl)):
                        if scene_pcl[k][4] == 5 and scene_pcl[k][2] >= 0.3-lidar_coordinates[2]:
                            scene_pcl[k][4] = 0

                    for k in range(len(dence_scene)):
                        if dence_scene[k][4] == 5 and dence_scene[k][2] >= 0.3-lidar_coordinates[2]:
                            dence_scene[k][4] = 0

                    # scene FOV
                    scene_pcl = add_space_for_spherical(scene_pcl)

                    scene_pcl, max_beta, min_beta = fill_spherical(scene_pcl)

                    train_scene, label_scene, scene_pcl = geometrical_front_view(scene_pcl, NUMROW, NUMCOLUMN, max_beta, min_beta)

                    print('scene FOV complete')
                    # dence FOV
                    dence_scene = add_space_for_spherical(dence_scene)

                    dence_scene, _, _ = fill_spherical(dence_scene)

                    train_dence, label_dence, dence_scene = geometrical_front_view(dence_scene, NUMROW, NUMCOLUMN, max_beta, min_beta)

                    print('dence FOV complete')
                    # moving object in scene
                    moving_objects_list = make_move_list(annotation, moving_objects)
                    # array for moving object pcl
                    moving_objects_pcl = [0] * len(moving_objects_list)
                    combine_moving_objects_pcl = np.array([])

                    # removing moving object and store to array
                    for o in range(len(moving_objects_list)):
                        scene_pcl, bbox = separate_bbox(scene_pcl, moving_objects_list[o], anno_info, lidar_coordinates)
                        moving_objects_pcl[o] = copy.deepcopy(bbox)
                        if o == 0:
                            combine_moving_objects_pcl = copy.deepcopy(bbox)
                        else:
                            combine_moving_objects_pcl = np.append(combine_moving_objects_pcl, copy.deepcopy(bbox), axis=0)

                    train_move, label_move, combine_moving_objects_pcl = geometrical_front_view(combine_moving_objects_pcl, NUMROW, NUMCOLUMN, max_beta, min_beta)

                    # add points which object covers
                    for row in range(NUMROW):
                        for column in range(NUMCOLUMN):
                            if label_move[row][column] != 6:
                                num_of_removed_points = len(combine_moving_objects_pcl[combine_moving_objects_pcl[:, 8] == row * NUMCOLUMN + column])
                                filling_part_pcl = dence_scene[dence_scene[:, 8] == row * NUMCOLUMN + column]
                                filling_part_pcl = filling_part_pcl[filling_part_pcl[:, 7] == label_dence[row][column]]

                                if len(filling_part_pcl) < num_of_removed_points:
                                    scene_pcl = np.append(scene_pcl, copy.deepcopy(filling_part_pcl), axis=0)
                                else:
                                    print('added normal')
                                    scene_pcl = np.append(scene_pcl, copy.deepcopy(filling_part_pcl[0:num_of_removed_points, :]), axis=0)

                    # direction of object movement
                    object_moved = [False] * len(moving_objects_list)
                    # from previous scene
                    if f != 0:
                        previous_scene = np.load(DATA_PATH + directory_list[directory_index] + '/' + sequence_list[directory_index][sequence_index] + '/' + current_directory[f-1], allow_pickle=True)
                        previous_annotation = previous_scene['anno']
                        previous_transformation_matrix = previous_scene['transform_matrix']

                        for o in range(len(moving_objects_list)):
                            find = False
                            for d in range(len(previous_annotation)):
                                if previous_annotation[d]['track_label_uuid'] == moving_objects_list[o]['track_label_uuid']:
                                    # position in previous local scene
                                    position[0][0] = previous_annotation[d]['center']['x']
                                    position[1][0] = previous_annotation[d]['center']['y']
                                    position[2][0] = previous_annotation[d]['center']['z']
                                    find = True
                                    break
                            if not find:
                                continue
                            else:
                                # position in global
                                position = np.dot(previous_transformation_matrix, position)
                                # position in local scene
                                position = np.dot(inversion_transform_matrix, position)

                                dx = moving_objects_list[o]['center']['x'] - position[0][0]
                                dy = moving_objects_list[o]['center']['y'] - position[1][0]

                                object_o = moving_objects_pcl[o]

                                object_o[:, 0] = object_o[:, 0] + dx*TIME
                                object_o[:, 1] = object_o[:, 1] + dy*TIME

                                moving_objects_pcl[o] = copy.deepcopy(object_o)

                                object_moved[o] = True

                    # from next scene
                    if f != len(current_directory)-1:
                        next_scene = np.load(DATA_PATH + directory_list[directory_index] + '/' + sequence_list[directory_index][sequence_index] + '/' + current_directory[f + 1], allow_pickle=True)
                        next_annotation = next_scene['anno']
                        next_transformation_matrix = next_scene['transform_matrix']

                        for o in range(len(moving_objects_list)):
                            if object_moved[o]:
                                continue
                            find = False
                            for d in range(len(next_annotation)):
                                if next_annotation[d]['track_label_uuid'] == moving_objects_list[o]['track_label_uuid']:
                                    # position in previous local scene
                                    position[0][0] = next_annotation[d]['center']['x']
                                    position[1][0] = next_annotation[d]['center']['y']
                                    position[2][0] = next_annotation[d]['center']['z']
                                    find = True
                                    break
                            if not find:
                                continue
                            else:
                                # position in global
                                position = np.dot(next_transformation_matrix, position)
                                # position in local scene
                                position = np.dot(inversion_transform_matrix, position)

                                dx = position[0][0] - moving_objects_list[o]['center']['x']
                                dy = position[1][0] - moving_objects_list[o]['center']['y']

                                object_o = moving_objects_pcl[o]
                                object_o[:, 0] = object_o[:, 0] + dx*TIME
                                object_o[:, 1] = object_o[:, 1] + dy*TIME

                                moving_objects_pcl[o] = copy.deepcopy(object_o)

                                object_moved[o] = True

                    print('Object moved')

                    for o in range(len(moving_objects_pcl)):
                        train_scene, label_scene, scene_pcl = geometrical_front_view(scene_pcl, NUMROW, NUMCOLUMN, max_beta, min_beta)

                        train_scene, label_scene = smooth_out(train_scene, label_scene)

                        object_pcl = moving_objects_pcl[o]

                        if len(object_pcl) == 0:
                            continue

                        object_pcl, _, _ = fill_spherical(object_pcl)

                        train_object, label_object, object_pcl = geometrical_front_view(object_pcl, NUMROW, NUMCOLUMN, max_beta, min_beta)

                        train_object, label_object = smooth_out(train_object, label_object)

                        first_part = True
                        visible_sample = np.array([])

                        for row in range(NUMROW):
                            for column in range(NUMCOLUMN):
                                if label_object[row][column] != 6:
                                    if train_scene[row][column][0] > train_object[row][column][0] or label_scene[row][column] == 6:
                                        scene_pcl = scene_pcl[scene_pcl[:, 8] != row * NUMCOLUMN + column]
                                        visible_part = object_pcl[object_pcl[:, 8] == row * NUMCOLUMN + column]
                                        if first_part:
                                            first_part = False
                                            visible_sample = visible_part
                                        else:
                                            visible_sample = np.append(visible_sample, visible_part, axis=0)
                        if len(visible_sample.shape) == 2:
                            scene_pcl = np.append(scene_pcl, copy.deepcopy(visible_sample), axis=0)

                    ### MAKING NEW FoV
                    scene_train_new, scene_label_new, scene_pcl = geometrical_front_view(scene_pcl, NUMROW, NUMCOLUMN, max_beta, min_beta)

                    ### SAVING DATA
                    png_name = file.split('.')[0]

                    scene_train_new[:, :, 0] = scene_train_new[:, :, 0] / (np.sqrt(CROP_X ** 2 + CROP_Y ** 2 + CROP_Z ** 2))
                    if np.max(scene_train_new[:, :, 0]) > 1:
                        print('NORMALIZATION ERROR DISTANCE', np.max(scene_train_new[:, :, 0]))
                    if np.max(scene_train_new[:, :, 1]) > 1:
                        print('NORMALIZATION ERROR INTENZITY', np.max(scene_train_new[:, :, 1]))

                    create_image(scene_label_new, SAVE_OUTPUT_FOLDER + 'picture/' + png_name + '.png')
                    # SAVE EACH NPZ
                    np.savez(SAVE_OUTPUT_FOLDER + 'data/' + file, train=scene_train_new, label=scene_label_new)

