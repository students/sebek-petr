import numpy as np
import os
from PIL import Image
import copy

RGB_CLASS = np.array([[255, 0, 0], [255, 255, 0], [255, 0, 255], [0, 0, 255], [123, 123, 123], [0, 255, 0], [0, 0, 0], [0, 191, 255]])

DATA_PATH = ''      # address with training data
JSON_PATH = ''      # address with json files

TRAIN_DIRECTORY = ['train1', 'train2', 'train3', 'train4']
TRAIN_SEQUENCES = [['f3fb839e-0aa2-342b-81c3-312b80be44f9', 'b3def699-884b-3c9e-87e1-1ab76c618e0b', '11953248-1195-1195-1195-511954366464', '26d141ec-f952-3908-b4cc-ae359377424e', '10b8dee6-778f-33e4-a946-d842d2d9c3d7', 'c6911883-1843-3727-8eaa-41dc8cda8993', '230970eb-dc2e-3133-b252-ff3c6f5d4284', '8a15674a-ae5c-38e2-bc4b-f4156d384072', '88538208-8853-8853-8853-388539396096', '64c12551-adb9-36e3-a0c1-e43a0e9f3845', 'dcdcd8b3-0ba1-3218-b2ea-7bb965aad3f0', 'e17eed4f-3ffd-3532-ab89-41a3f24cf226'],
                   ['f0826a9f-f46e-3c27-97af-87a77f7899cd', 'fb471bd6-7c81-3d93-ad12-ac54a28beb84', '53213cf0-540b-3b5a-9900-d24d1d41bda0', 'ebe7a98b-d383-343b-96d6-9e681e2c6a36', '02cf0ce1-699a-373b-86c0-eb6fd5f4697a', 'fa0b626f-03df-35a0-8447-021088814b8b', '38b2c7ef-069b-3d9d-bbeb-8847b8c89fb6', '0ef28d5c-ae34-370b-99e7-6709e1c4b929'],
                   ['aebe6aaa-6a95-39e6-9a8d-06103141fcde', 'ff78e1a3-6deb-34a4-9a1f-b85e34980f06', '08a8b7f0-c317-3bdb-b3dc-b7c9b6d033e2', '919be600-da69-3f09-b0fd-f42f7eb2e097', 'de777454-df62-3d5a-a1ce-2edb5e5d4922', 'd4d9e91f-0f8e-334d-bd0e-0d062467308a', '99c45b6e-6fc7-39b8-80d7-727c485fb561', '10f92308-e06e-3725-a302-4b09e6e790ad', '6c739f57-96d0-33e6-972d-af29cc527e1f', '52af191b-ba56-326c-b569-e37790db40f3'],
                   ['15c802a9-0f0e-3c87-b516-a3fa02f1ecb0', '2bc6a872-9979-3493-82eb-fb55407473c9', '49d66e75-3ce6-316b-b589-f659c7ef5e6d']]

SAVE_OUTPUT_FOLDER = ''     # address of directory to save data


def create_image(labels, filename):
    """
    Function, which create Image of classes.
    :param labels: numpy 2D array with classes
    :param filename: string, name of the image
    """

    columns = len(labels[0])
    lines = len(labels)
    rgb = np.zeros(3 * columns * lines).reshape(lines, columns, 3)

    for i in range(lines):
        for j in range(columns):
            labels_idx = int(labels[i][j])
            rgb[i][j] = RGB_CLASS[labels_idx]

    rgb = np.uint8(rgb)
    img = Image.fromarray(rgb, 'RGB')
    img.save(filename)


if __name__ == '__main__':
    '''
    making driveable area from scenes
    '''
    directory_list = TRAIN_DIRECTORY
    sequence_list = TRAIN_SEQUENCES
    for directory_index in range(len(directory_list)):
        for sequence_index in range(len(sequence_list[directory_index])):

            current_directory = os.listdir(DATA_PATH + directory_list[directory_index] + '/' + sequence_list[directory_index][sequence_index])
            print(DATA_PATH + directory_list[directory_index] + '/' + sequence_list[directory_index][sequence_index])

            current_directory.sort()

            track_list = np.array([])
            position = np.zeros((4, 1))
            position[3][0] = 1
            for file in current_directory:
                if file.endswith('.npz'):
                    data = np.load(DATA_PATH + directory_list[directory_index] + '/' + sequence_list[directory_index][sequence_index] + '/' + file, allow_pickle=True)
                    annotation = data['anno']
                    transform_matrix = data['transform_matrix']
                    for anno in annotation:

                        row = np.where(track_list == anno['track_label_uuid'])

                        if len(row[0]) == 0:
                            position[0][0] = anno['center']['x']
                            position[1][0] = anno['center']['y']
                            position[2][0] = anno['center']['z']
                            global_position = np.dot(transform_matrix, position)
                            track_block = np.array([[anno['track_label_uuid'], anno['label_class'], [global_position[0][0], global_position[1][0]], False]])
                            if len(track_list) == 0:
                                track_list = copy.deepcopy(track_block)
                            else:
                                track_list = np.append(track_list, copy.deepcopy(track_block), axis=0)
                        else:
                            position[0][0] = anno['center']['x']
                            position[1][0] = anno['center']['y']
                            position[2][0] = anno['center']['z']
                            global_position = np.dot(transform_matrix, position)
                            row = row[0][0]
                            if abs(track_list[row][2][0]-global_position[0][0]) > 0.5 or abs(track_list[row][2][1]-global_position[1][0]) > 0.5:
                                track_list[row][3] = True

            track_list = track_list[track_list[:, 3] == True]
            np.savez(SAVE_OUTPUT_FOLDER + directory_list[directory_index] + '/' + sequence_list[directory_index][sequence_index] + '_moving_objects.npz', objects=track_list)

