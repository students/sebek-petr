import numpy as np
import json
import copy
from scipy.spatial.transform import Rotation as R

from cut_bbox import cut_bounding_box


HIGHLIGHT = False


def make_dictionary(annotation_array):
    center = {'x': annotation_array[0][0], 'y': annotation_array[0][1], 'z': annotation_array[0][2]}
    rotation = {'x': annotation_array[1][0], 'y': annotation_array[1][1], 'z': annotation_array[1][2], 'w': annotation_array[1][3]}
    annotation_dictionary = {'center': center, 'rotation': rotation, 'length': annotation_array[2][0],
                             'width': annotation_array[2][1], 'height': annotation_array[2][2], 'track_label_uuid':
                            annotation_array[3][0], 'timestamp': annotation_array[3][1], 'label_class': annotation_array[3][2]}
    return annotation_dictionary


def rotate_bounding_box(bbox_pcl, annotation, rotation=1):
    '''
    :param bbox_pcl: point cloud inside bounding box
    :param annotation: information about bounding box
    :param rotation: angle to rotate bounging box (in degrees)
    :return: rotated bounding box
    '''
    rotation = np.deg2rad(rotation)
    r = R.from_quat(annotation[1])
    rot_matrix = r.as_dcm()
    # print(rot_matrix)
    z_rot_matrix = np.array([[np.cos(rotation), -np.sin(rotation), 0],
                    [np.sin(rotation), np.cos(rotation), 0],
                    [0, 0, 1]])
    # print(z_rot_matrix)
    final_rotation = np.dot(rot_matrix, z_rot_matrix)
    ft = R.from_dcm(final_rotation)
    annotation[1] = ft.as_quat()
    tmp = np.array([[annotation[0][0]], [annotation[0][1]], [annotation[0][2]]])
    tmp = np.dot(z_rot_matrix, tmp)
    annotation[0][0] = tmp[0][0]
    annotation[0][1] = tmp[1][0]
    annotation[0][2] = tmp[2][0]
    for i in range(len(bbox_pcl)):
        tmp = np.array([[bbox_pcl[i][0]], [bbox_pcl[i][1]], [bbox_pcl[i][2]]])
        tmp = np.dot(z_rot_matrix, tmp)
        bbox_pcl[i][0] = tmp[0][0]
        bbox_pcl[i][1] = tmp[1][0]
        bbox_pcl[i][2] = tmp[2][0]

    return bbox_pcl, annotation


def check_bounding_box(scene_pcl, scene_anno, sample_pcl, sample_anno, lidar_coordinates):
    ok = True
    tmp = cut_bounding_box(scene_pcl, sample_anno)
    tmp = tmp[tmp[:, 7] != 5]
    if len(tmp) > 0:
        ok = False

    if ok:
        for i in range(len(scene_anno)):
            tmp = cut_bounding_box(sample_pcl, scene_anno[i], lidar_coordinates)
            if len(tmp) > 0:
                ok = False
                break
    return ok


def correct_height(scene_pcl, sample_pcl, sample_anno):
    tmp = np.array([])
    radius = 0.1

    while len(tmp) == 0:
        tmp = scene_pcl[(scene_pcl[:, 0]-sample_anno[0][0])**2+(scene_pcl[:, 1]-sample_anno[0][1])**2 <= radius**2]
        tmp = tmp[tmp[:, 7] == 5]
        radius += 0.1

    road_level = np.mean(tmp, axis=0)[2]
    new_bbox_z_level = road_level + sample_anno[2][2]/2
    z_move = new_bbox_z_level - sample_anno[0][2]
    sample_pcl[:, 2] += z_move
    sample_anno[0][2] = new_bbox_z_level
    if radius > 5:
        ok = False
    else:
        ok = True
    return sample_pcl, sample_anno, ok


def find_possible_places(point_cloud, transformation_matrix, scene_annotation, sample_data, map_data, lidar_coordinates):
    output_pcl = []
    output_annotation = []

    ### LOADING DATA
    # sample point cloud
    sample_pcl = sample_data['pcl']
    sample_annotation = sample_data['anno']

    # driveable area map
    map_move = map_data['move']
    map = map_data['map']

    for point in point_cloud:
        if point[4] == 5:
            continue
        x = np.array([[point[0]], [point[1]], [point[2]], [1.]])
        global_position = np.dot(transformation_matrix, x)
        global_position = global_position - map_move
        if map[int(global_position[0][0])][int(global_position[1][0])] == 1:
            map[int(global_position[0][0])][int(global_position[1][0])] = 4

    for rot in range(360):
        on_road = True
        sample_pcl, sample_annotation = rotate_bounding_box(sample_pcl, sample_annotation)
        for sample_point in sample_pcl:
            x = np.array([[sample_point[0]], [sample_point[1]], [sample_point[2]], [1.]])
            global_position = np.dot(transformation_matrix, x)
            global_position = global_position - map_move
            if map[int(global_position[0][0])][int(global_position[1][0])] == 0:
                on_road = False
                break

        if on_road:
            sample_pcl, sample_annotation, near_road = correct_height(point_cloud, sample_pcl, sample_annotation)
            if near_road == False:
                print('Road too far')
                continue
            sample_annotation_dict = make_dictionary(sample_annotation)
            ok = check_bounding_box(point_cloud, scene_annotation, sample_pcl, sample_annotation_dict, lidar_coordinates)
            if ok:
                if output_pcl == None:
                    output_pcl = [copy.deepcopy(sample_pcl)]
                    output_annotation = [copy.deepcopy(sample_annotation_dict)]
                else:
                    output_pcl.append(copy.deepcopy(sample_pcl))
                    output_annotation.append(copy.deepcopy(sample_annotation_dict))

    return output_pcl, output_annotation