import json
import os
import numpy as np
import copy
from PIL import Image

from closing import *
from find_spot import *

NUMCOLUMN = 360 * 4
NUMROW = 112

CROP = True

CROP_X = 100
CROP_Y = 50
CROP_Z = 10

RGB_CLASS = np.array([[255, 0, 0], [255, 255, 0], [255, 0, 255], [0, 0, 255], [123, 123, 123], [0, 255, 0], [0, 0, 0]])

DATA_PATH = ''      # address of directory with point clouds data
JSON_PATH = ''      # address of directory with json

SAMPLE_PATH = ''    # address of directory with objects point clouds

TRAIN_DIRECTORY = ['train1', 'train2', 'train3', 'train4']
TRAIN_SEQUENCES = [['f3fb839e-0aa2-342b-81c3-312b80be44f9', 'b3def699-884b-3c9e-87e1-1ab76c618e0b', '11953248-1195-1195-1195-511954366464', '26d141ec-f952-3908-b4cc-ae359377424e', '10b8dee6-778f-33e4-a946-d842d2d9c3d7', 'c6911883-1843-3727-8eaa-41dc8cda8993', '230970eb-dc2e-3133-b252-ff3c6f5d4284', '8a15674a-ae5c-38e2-bc4b-f4156d384072', '88538208-8853-8853-8853-388539396096', '64c12551-adb9-36e3-a0c1-e43a0e9f3845', 'dcdcd8b3-0ba1-3218-b2ea-7bb965aad3f0', 'e17eed4f-3ffd-3532-ab89-41a3f24cf226'],
                   ['f0826a9f-f46e-3c27-97af-87a77f7899cd', 'fb471bd6-7c81-3d93-ad12-ac54a28beb84', '53213cf0-540b-3b5a-9900-d24d1d41bda0', 'ebe7a98b-d383-343b-96d6-9e681e2c6a36', '02cf0ce1-699a-373b-86c0-eb6fd5f4697a', 'fa0b626f-03df-35a0-8447-021088814b8b', '38b2c7ef-069b-3d9d-bbeb-8847b8c89fb6', '0ef28d5c-ae34-370b-99e7-6709e1c4b929'],
                   ['aebe6aaa-6a95-39e6-9a8d-06103141fcde', 'ff78e1a3-6deb-34a4-9a1f-b85e34980f06', '08a8b7f0-c317-3bdb-b3dc-b7c9b6d033e2', '919be600-da69-3f09-b0fd-f42f7eb2e097', 'de777454-df62-3d5a-a1ce-2edb5e5d4922', 'd4d9e91f-0f8e-334d-bd0e-0d062467308a', '99c45b6e-6fc7-39b8-80d7-727c485fb561', '10f92308-e06e-3725-a302-4b09e6e790ad', '6c739f57-96d0-33e6-972d-af29cc527e1f', '52af191b-ba56-326c-b569-e37790db40f3'],
                   ['15c802a9-0f0e-3c87-b516-a3fa02f1ecb0', '2bc6a872-9979-3493-82eb-fb55407473c9', '49d66e75-3ce6-316b-b589-f659c7ef5e6d']]

SAVE_OUTPUT_FOLDER = '' # address of directory for saving augmented data

def create_image(labels, filename):
    """
    Function, which create Image of classes.
    :param labels: numpy 2D array with classes
    :param filename: string, name of the image
    """

    columns = len(labels[0])
    lines = len(labels)
    rgb = np.zeros(3 * columns * lines).reshape(lines, columns, 3)

    for i in range(lines):
        for j in range(columns):
            labels_idx = int(labels[i][j])
            rgb[i][j] = RGB_CLASS[labels_idx]

    rgb = np.uint8(rgb)
    img = Image.fromarray(rgb, 'RGB')
    img.save(filename)
    return


def add_space_for_spherical(point_cloud):
    '''
    :param point_cloud: original point-cloud as 2D array
    :return: point-cloud as 2D array with space for spherical coordination
    '''
    points_num = len(point_cloud)
    out = np.ones((points_num, 9))*-6
    out[:, 0:3] = point_cloud[:, 0:3]    # X, Y, Z
    out[:, 6:8] = point_cloud[:, 3:5]    # Intensity, Label
    return out  # X, Y, Z, r, alpha, beta, Intensity, Label, index


def fill_spherical(point_cloud):         # X, Y, Z, r, alpha, beta, Intensity, Label
    '''
    :param point_cloud: point-cloud as 2D array
    :return: point-cloud as 2D array with computed spherical coordination
    '''
    point_cloud[:, 3] = np.sqrt(point_cloud[:, 0] ** 2 + point_cloud[:, 1] ** 2 + point_cloud[:, 2] ** 2)  # r (0;infty)
    point_cloud[:, 4] = np.arctan2(point_cloud[:, 1], point_cloud[:, 0]) + np.pi  # alpha (0;2pi)
    point_cloud[:, 5] = np.arccos(point_cloud[:, 2] / point_cloud[:, 3])  # beta

    min_beta_geometrical = np.min(point_cloud[:, 5])
    max_beta_geometrical = np.max(point_cloud[:, 5])

    return point_cloud, max_beta_geometrical, min_beta_geometrical


def geometrical_front_view(point_cloud, num_row, num_column, max_beta, min_beta): # X, Y, Z, r, alpha, beta, Intensity, Label
    '''
    :param point_cloud: point-cloud as 2D array
    :param num_row: front view row resolution
    :param num_column: rront view column resolution
    :param max_beta: maximal elevation angle in original point-cloud
    :param min_beta: minimal elevation angle in original point-cloud
    :return: front view and labels for front view
    '''
    point_num = len(point_cloud)
    dbeta = (max_beta-min_beta) / num_row       # resolution in row
    daplha = 2 * math.pi / num_column           # resolution in column
    label = np.ones((num_row, num_column)) * 6
    train = np.zeros((num_row, num_column, 3))  # distance, intensity, arrived?

    for i in range(point_num):
        point = point_cloud[i]

        row = int((point[5] - min_beta) / dbeta)
        if point[5] == max_beta:
            row = num_row - 1

        if point[5] > max_beta:
            continue

        if point[5] < min_beta:
            continue

        column = int(point[4] / daplha)

        if label[row][column] != 6:

            point_cloud[i][8] = row * NUMCOLUMN + column

            if train[row][column][0] > point[3]:
                label[row][column] = point[7]  # label

                train[row][column][0] = point[3]  # distance
                train[row][column][1] = point[6]  # intensity
                train[row][column][2] = 1  # arrived

        else:
            label[row][column] = point[7]     # label

            train[row][column][0] = point[3]     # distance
            train[row][column][1] = point[6]     # intensity
            train[row][column][2] = 1            # arrived

            point_cloud[i][8] = row * NUMCOLUMN + column

    return train, label, point_cloud


if __name__ == '__main__':
    scene_directory_list = TRAIN_DIRECTORY
    sequence_list = TRAIN_SEQUENCES
    map_data = np.load('road_map.npz', allow_pickle=True)
    for directory_index in range(len(scene_directory_list)):
        for sequence_index in range(len(sequence_list[directory_index])):

            current_directory = os.listdir(DATA_PATH + scene_directory_list[directory_index] + '/' + sequence_list[directory_index][sequence_index])
            print(DATA_PATH + scene_directory_list[directory_index] + '/' + sequence_list[directory_index][sequence_index])

            with open(JSON_PATH + scene_directory_list[directory_index] + '/' + sequence_list[directory_index][sequence_index] + '/vehicle_calibration_info.json') as json_data_file:
                lidar_info = json.load(json_data_file)
                lidar_coordinates = (np.array(lidar_info['vehicle_SE3_down_lidar_']['translation']) +
                                     np.array(lidar_info['vehicle_SE3_up_lidar_']['translation'])) / 2

            for file in current_directory:
                if file.endswith('.npz'):
                    print(DATA_PATH + scene_directory_list[directory_index] + '/' + sequence_list[directory_index][sequence_index] + '/' + file)
                    if os.path.exists(SAVE_OUTPUT_FOLDER + 'data/' + file):
                        print('Already done')
                        continue

                    #### SCENE POINT CLOUD
                    # original point cloud
                    data = np.load(DATA_PATH + scene_directory_list[directory_index] + '/' + sequence_list[directory_index][sequence_index] + '/' + file, allow_pickle=True)
                    scene_pcl = data['pcl']
                    scene_annotation = data['anno']
                    transformation_matrix = data['transform_matrix']
                    ###

                    ### SCENE POINT CLOUD CALIBRATION
                    scene_pcl[:, 0:3] = scene_pcl[:, 0:3] - lidar_coordinates

                    # CROP DATA
                    if CROP:
                        scene_pcl = scene_pcl[scene_pcl[:, 2] < CROP_Z]
                        scene_pcl = scene_pcl[scene_pcl[:, 0] < CROP_X]
                        scene_pcl = scene_pcl[scene_pcl[:, 0] > -CROP_X]
                        scene_pcl = scene_pcl[scene_pcl[:, 1] < CROP_Y]
                        scene_pcl = scene_pcl[scene_pcl[:, 1] > -CROP_Y]
                    else:
                        scene_pcl = scene_pcl

                    for k in range(len(scene_pcl)):
                        if scene_pcl[k][4] == 5 and scene_pcl[k][2] >= 0.3 - lidar_coordinates[2]:
                            scene_pcl[k][4] = 0
                    ###

                    ### SCENE FIELD OF VIEW (smooth)
                    scene_pcl = add_space_for_spherical(scene_pcl)

                    scene_pcl, max_beta, min_beta = fill_spherical(scene_pcl)

                    scene_train, scene_label, scene_pcl = geometrical_front_view(scene_pcl, NUMROW, NUMCOLUMN, max_beta, min_beta)

                    scene_train, scene_label = smooth_out(scene_train, scene_label)
                    ###

                    scene_pcl_backup = copy.deepcopy(scene_pcl)

                    ### SAMPLE POINT CLOUD
                    # choose from random train
                    sample_directory_index = np.random.randint(3, size=(1))[0]
                    if sample_directory_index == directory_index:
                        if sample_directory_index + 1 >= len(TRAIN_DIRECTORY):
                            sample_directory_index = 0
                        else:
                            sample_directory_index += 1

                    sample_directory_list = os.listdir(SAMPLE_PATH + TRAIN_DIRECTORY[sample_directory_index])

                    match_find = False
                    while not match_find:
                        sample_file = sample_directory_list[np.random.randint(len(sample_directory_list), size=1)[0]]
                        if sample_file.endswith('.npz'):

                            ### LOADING SAMPLE POINT CLOUD
                            print(SAMPLE_PATH + TRAIN_DIRECTORY[sample_directory_index] + "/" + sample_file)
                            sample_data = np.load(SAMPLE_PATH + TRAIN_DIRECTORY[sample_directory_index] + "/" + sample_file, allow_pickle=True)

                            possible_sample_pcl, possible_sample_annotation = find_possible_places(scene_pcl, transformation_matrix, scene_annotation,
                                                              sample_data, map_data, lidar_coordinates)

                            for sample_index in range(len(possible_sample_pcl)):
                                sample_pcl = possible_sample_pcl[sample_index]
                                sample_annotation = possible_sample_annotation[sample_index]
                                scene_pcl = copy.deepcopy(scene_pcl_backup)

                                sample_pcl = add_space_for_spherical(sample_pcl)

                                sample_pcl, _, _ = fill_spherical(sample_pcl)

                                sample_train, sample_label, sample_pcl = geometrical_front_view(sample_pcl, NUMROW, NUMCOLUMN, max_beta, min_beta)

                                sample_train, sample_label = smooth_out(sample_train, sample_label)

                                first_part = True
                                visible_sample = np.array([])

                                for row in range(NUMROW):
                                    for column in range(NUMCOLUMN):
                                        if sample_label[row][column] != 6:
                                            if scene_train[row][column][0] > sample_train[row][column][0] or scene_label[row][column] == 6:
                                                scene_pcl = scene_pcl[scene_pcl[:, 8] != row * NUMCOLUMN + column]
                                                visible_part = sample_pcl[sample_pcl[:, 8] == row * NUMCOLUMN + column]
                                                if first_part:
                                                    first_part = False
                                                    visible_sample = visible_part
                                                else:
                                                    visible_sample = np.append(visible_sample, visible_part, axis=0)

                                if len(visible_sample) == 0:
                                    print('NO VISIBLE PART OF OBJECT')
                                elif len(visible_sample) < 20:
                                    print('VISIBLE', len(visible_sample), 'POINTS FROM OBJECT. IT CAN BE HARD CASE TRY ANNOTHER')
                                else:
                                    print('VISIBLE', len(visible_sample), 'POINTS FROM OBJECT. MOVING TO ANOTHER SCENE')
                                    match_find = True
                                    scene_pcl = np.append(scene_pcl, visible_sample, axis=0)

                                    ### MAKING NEW FoV
                                    scene_train_new, scene_label_new, scene_pcl = geometrical_front_view(scene_pcl, NUMROW, NUMCOLUMN, max_beta, min_beta)

                                    ### SAVING DATA
                                    png_name = file.split('.')[0]

                                    scene_train_new[:, :, 0] = scene_train_new[:, :, 0] / (np.sqrt(CROP_X ** 2 + CROP_Y ** 2 + CROP_Z ** 2))
                                    if np.max(scene_train_new[:, :, 0]) > 1:
                                        print('NORMALIZATION ERROR DISTANCE', np.max(scene_train_new[:, :, 0]))
                                    if np.max(scene_train_new[:, :, 1]) > 1:
                                        print('NORMALIZATION ERROR INTENZITY', np.max(scene_train_new[:, :, 1]))

                                    create_image(scene_label_new, SAVE_OUTPUT_FOLDER + 'picture/' + png_name + '.png')
                                    # SAVE EACH NPZ
                                    np.savez(SAVE_OUTPUT_FOLDER + 'data/' + file, train=scene_train_new, label=scene_label_new)
                                    np.savez(SAVE_OUTPUT_FOLDER + 'check/' + file, pcl=visible_sample,
                                             anno=sample_annotation)
                                    break
