import numpy as np
import os
from PIL import Image

RGB_CLASS = np.array([[0, 0, 128], [0, 191, 255], [255, 0, 255], [0, 255, 0], [123, 123, 123], [0, 255, 0], [0, 0, 255]])

DATA_PATH = ''      # address of dictionary with drivable area from single sequences


def create_image(labels, filename):
    """
    Function, which create Image of classes.
    :param labels: numpy 2D array with classes
    :param filename: string, name of the image
    """

    columns = len(labels[0])
    lines = len(labels)
    rgb = np.zeros(3 * columns * lines).reshape(lines, columns, 3)

    for i in range(lines):
        for j in range(columns):
            labels_idx = int(labels[i][j])
            rgb[i][j] = RGB_CLASS[labels_idx]

    rgb = np.uint8(rgb)
    img = Image.fromarray(rgb, 'RGB')
    img.save(filename)


if __name__ == '__main__':
    '''
    making driveable area from scenes
    '''
    directory_list = os.listdir(DATA_PATH)

    map_1 = np.load(DATA_PATH + '/' + directory_list[0], allow_pickle=True)
    move_1 = map_1['move']
    map_1 = map_1['map']
    map_1 = map_1[:, :, 0]

    for row in range(map_1.shape[0]):
        for column in range(map_1.shape[1]):
            map_1[row][column] = 0

    for i in range(1, len(directory_list)):
        file = directory_list[i]
        if file.endswith('.npz'):

            print(DATA_PATH + '/' + file)
            map_2 = np.load(DATA_PATH + '/' + file, allow_pickle=True)
            move_2 = map_2['move']
            map_2 = map_2['map']

            if move_1[0][0] < move_2[0][0]:
                min_x = move_1[0][0]
            else:
                min_x = move_2[0][0]

            if move_1[0][0]+map_1.shape[0] > move_2[0][0]+map_2.shape[0]:
                max_x = move_1[0][0] + map_1.shape[0]
            else:
                max_x = move_2[0][0] + map_2.shape[0]

            if move_1[1][0] < move_2[1][0]:
                min_y = move_1[1][0]
            else:
                min_y = move_2[1][0]

            if move_1[1][0]+map_1.shape[1] > move_2[1][0]+map_2.shape[1]:
                max_y = move_1[1][0] + map_1.shape[1]
            else:
                max_y = move_2[1][0] + map_2.shape[1]

            combine_map = np.zeros((max_x-min_x, max_y-min_y))

            m1_delta_x = move_1[0][0] - min_x
            m1_delta_y = move_1[1][0] - min_y
            for row in range(map_1.shape[0]):
                for column in range(map_1.shape[1]):
                    if map_1[row][column] == 1:
                        combine_map[row+m1_delta_x][column+m1_delta_y] = 1

            m2_delta_x = move_2[0][0] - min_x
            m2_delta_y = move_2[1][0] - min_y
            for row in range(map_2.shape[0]):
                for column in range(map_2.shape[1]):
                    if map_2[row][column][0] == 1:
                        if combine_map[row+m2_delta_x][column+m2_delta_y] == 1:
                            combine_map[row+m2_delta_x][column+m2_delta_y] = 3
                        else:
                            combine_map[row+m2_delta_x][column+m2_delta_y] = 2

            create_image(combine_map, 'combination-' + str(i) + '.png')

            for row in range(combine_map.shape[0]):
                for column in range(combine_map.shape[1]):
                    if combine_map[row][column] != 0:
                        combine_map[row][column] = 1

            np.savez('combination-' + str(i), move=np.array([[min_x], [min_y], [0], [1]]), map=combine_map)
            map_1 = combine_map
            move_1 = np.array([[min_x], [min_y], [0], [1]])

            if i == len(directory_list)-1:
                create_image(combine_map, 'road_map.png')
                np.savez('road_map', move=np.array([[min_x], [min_y], [0], [1]]),
                         map=combine_map)