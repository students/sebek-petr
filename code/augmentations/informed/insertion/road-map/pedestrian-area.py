import numpy as np
from PIL import Image


RGB_CLASS = np.array([[255, 0, 0], [255, 255, 0], [255, 0, 255], [0, 0, 255], [123, 123, 123], [0, 255, 0], [0, 0, 0]])


def create_image(labels, filename):
    """
    Function, which create Image of classes.
    :param labels: numpy 2D array with classes
    :param filename: string, name of the image
    """

    columns = len(labels[0])
    lines = len(labels)
    rgb = np.zeros(3 * columns * lines).reshape(lines, columns, 3)

    for i in range(lines):
        for j in range(columns):
            labels_idx = int(labels[i][j])
            rgb[i][j] = RGB_CLASS[labels_idx]

    rgb = np.uint8(rgb)
    img = Image.fromarray(rgb, 'RGB')
    img.save(filename)
    return


if __name__ == '__main__':
    map_data = np.load('road_map.npz', allow_pickle=True)
    map = map_data['map']
    move = map_data['move']
    create_image(map, 'road_map.png')
    for row in range(map.shape[0]):
        for column in range(map.shape[1]):
            if map[row][column] == 1:
                for d in range(-2, 3, 1):
                    if (row + d > 0) and (row + d < map.shape[0]) and (map[row + d][column] == 0):
                        map[row + d][column] = 2
                    if (column + d > 0) and (column + d < map.shape[1]) and (map[row][column + d] == 0):
                        map[row][column + d] = 2
    create_image(map, 'road_sidewalk_map.png')
    np.savez('road_sidewalk_map', move=move, map=map)