import numpy as np
import os
from PIL import Image
import math

RGB_CLASS = np.array([[0, 0, 128], [0, 191, 255], [255, 0, 255], [0, 255, 0], [123, 123, 123], [0, 255, 0], [0, 0, 255]])

DATA_PATH = ''      # address of dictionary with training data

ANNOTATION = False

def create_image(labels, filename):
    """
    Function, which create Image of classes.
    :param labels: numpy 2D array with classes
    :param filename: string, name of the image
    """

    columns = len(labels[0])
    lines = len(labels)
    rgb = np.zeros(3 * columns * lines).reshape(lines, columns, 3)

    for i in range(lines):
        for j in range(columns):
            labels_idx = int(labels[i][j])
            rgb[i][j] = RGB_CLASS[labels_idx]

    rgb = np.uint8(rgb)
    img = Image.fromarray(rgb, 'RGB')
    img.save(filename)


if __name__ == '__main__':
    '''
    making driveable area from scenes
    '''
    directory_list = os.listdir(DATA_PATH)

    for directory in range(0, len(directory_list)):
        current_directory = os.listdir(DATA_PATH + "/" + directory_list[directory])

        max_global_x = -math.inf
        min_global_x = math.inf
        max_global_y = -math.inf
        min_global_y = math.inf

        for file in current_directory:
            if file.endswith('.npz'):

                # print(DATA_PATH + "/" + directory_list[directory] + "/" + file)
                data = np.load(DATA_PATH + "/" + directory_list[directory] + "/" + file, allow_pickle=True)
                point_cloud = data['pcl']
                transformation_matrix = data['transform_matrix']

                for i in range(len(point_cloud)):
                    point = point_cloud[i]
                    x = np.array([[point[0]], [point[1]], [point[2]], [1.]])
                    global_position = np.dot(transformation_matrix, x)

                    if global_position[0][0] > max_global_x:
                        max_global_x = global_position[0][0]
                    if global_position[0][0] < min_global_x:
                        min_global_x = global_position[0][0]
                    if global_position[1][0] > max_global_y:
                        max_global_y = global_position[1][0]
                    if global_position[1][0] < min_global_y:
                        min_global_y = global_position[1][0]
        print(directory_list[directory])
        print('LEFT TOP: [', min_global_x, ',', min_global_y, ']')
        print('RIGHT BOTTOM: [', max_global_x, ',', max_global_y, ']')

        min_global_x = int(min_global_x)
        min_global_y = int(min_global_y)

        max_global_y = int(max_global_y) + 1
        max_global_x = int(max_global_x) + 1

        size_x = int(max_global_x-min_global_x)

        size_y = int(max_global_y-min_global_y)

        print(size_x, size_y)

        scene_driveable_area = np.zeros((size_x, size_y, 2))

        for file in current_directory:
            if file.endswith('.npz'):

                # print(DATA_PATH + "/" + directory_list[directory] + "/" + file)
                data = np.load(DATA_PATH + "/" + directory_list[directory] + "/" + file, allow_pickle=True)
                point_cloud = data['pcl']
                transformation_matrix = data['transform_matrix']

                for i in range(len(point_cloud)):
                    point = point_cloud[i]
                    if point[4] != 5:
                        continue
                    x = np.array([[point[0]], [point[1]], [point[2]], [1.]])
                    global_position = np.dot(transformation_matrix, x)

                    position_x = global_position[0][0] - min_global_x
                    position_y = global_position[1][0] - min_global_y
                    if position_x < 0 or position_y < 0:
                        print(DATA_PATH + "/" + directory_list[directory] + "/" + file)
                        print('indexing error')

                    scene_driveable_area[int(position_x)][int(position_y)][0] = 1
                    scene_driveable_area[int(position_x)][int(position_y)][1] = global_position[2][0]

        create_image(scene_driveable_area[:, :, 0], 'picture/' + directory_list[directory] + '.png')
        np.savez('npz/' + directory_list[directory], move=np.array([[min_global_x], [min_global_y], [0], [1]]), map=scene_driveable_area)