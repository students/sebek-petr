import numpy as np
import os
import json
from scipy.spatial.transform import Rotation as R

DATA_PATH = ''      # address of directory with point clouds data
JSON_PATH = ''      # address of directory with json
SAVE_PATH = ''      # address of directory for saving augmented data

TRAIN_DIRECTORY = ['train1', 'train2', 'train3', 'train4']
TRAIN_SEQUENCES = [['f3fb839e-0aa2-342b-81c3-312b80be44f9', 'b3def699-884b-3c9e-87e1-1ab76c618e0b', '11953248-1195-1195-1195-511954366464', '26d141ec-f952-3908-b4cc-ae359377424e', '10b8dee6-778f-33e4-a946-d842d2d9c3d7', 'c6911883-1843-3727-8eaa-41dc8cda8993', '230970eb-dc2e-3133-b252-ff3c6f5d4284', '8a15674a-ae5c-38e2-bc4b-f4156d384072', '88538208-8853-8853-8853-388539396096', '64c12551-adb9-36e3-a0c1-e43a0e9f3845', 'dcdcd8b3-0ba1-3218-b2ea-7bb965aad3f0', 'e17eed4f-3ffd-3532-ab89-41a3f24cf226'],
                   ['f0826a9f-f46e-3c27-97af-87a77f7899cd', 'fb471bd6-7c81-3d93-ad12-ac54a28beb84', '53213cf0-540b-3b5a-9900-d24d1d41bda0', 'ebe7a98b-d383-343b-96d6-9e681e2c6a36', '02cf0ce1-699a-373b-86c0-eb6fd5f4697a', 'fa0b626f-03df-35a0-8447-021088814b8b', '38b2c7ef-069b-3d9d-bbeb-8847b8c89fb6', '0ef28d5c-ae34-370b-99e7-6709e1c4b929'],
                   ['aebe6aaa-6a95-39e6-9a8d-06103141fcde', 'ff78e1a3-6deb-34a4-9a1f-b85e34980f06', '08a8b7f0-c317-3bdb-b3dc-b7c9b6d033e2', '919be600-da69-3f09-b0fd-f42f7eb2e097', 'de777454-df62-3d5a-a1ce-2edb5e5d4922', 'd4d9e91f-0f8e-334d-bd0e-0d062467308a', '99c45b6e-6fc7-39b8-80d7-727c485fb561', '10f92308-e06e-3725-a302-4b09e6e790ad', '6c739f57-96d0-33e6-972d-af29cc527e1f', '52af191b-ba56-326c-b569-e37790db40f3'],
                   ['15c802a9-0f0e-3c87-b516-a3fa02f1ecb0', '2bc6a872-9979-3493-82eb-fb55407473c9', '49d66e75-3ce6-316b-b589-f659c7ef5e6d']]
CROP_X = 100
CROP_Y = 50
CROP_Z = 10


def cut_bounding_box(point_cloud, annotation, annotation_move=[0,0,0]):
    '''
    :param point_cloud: original point-cloud
    :param annotation: annotation of bounding box which should be cut out (dictionary)
    :return: annotations point-cloud
    '''
    xc = annotation['center']['x'] - annotation_move[0]
    yc = annotation['center']['y'] - annotation_move[1]
    zc = annotation['center']['z'] - annotation_move[2]
    Q1 = annotation['rotation']['x']
    Q2 = annotation['rotation']['y']
    Q3 = annotation['rotation']['z']
    Q4 = annotation['rotation']['w']
    length = annotation['length']
    width = annotation['width']
    height = annotation['height']

    r = R.from_quat([Q1, Q2, Q3, Q4])
    rot_matrix = r.as_dcm()

    bbox = point_cloud[
        rot_matrix[0][0] * point_cloud[:, 0] + rot_matrix[1][0] * point_cloud[:, 1] + rot_matrix[2][
            0] * point_cloud[:,
                 2] <
        rot_matrix[0][0] * (xc + rot_matrix[0][0] * length / 2) + rot_matrix[1][0] * (
                yc + rot_matrix[1][0] * length / 2) + rot_matrix[2][0] *
        (zc + rot_matrix[2][0] * length / 2)]

    bbox = bbox[
        rot_matrix[0][0] * bbox[:, 0] + rot_matrix[1][0] * bbox[:, 1] + rot_matrix[2][0] * bbox[:, 2] >
        rot_matrix[0][0] * (xc - rot_matrix[0][0] * length / 2) + rot_matrix[1][0] * (
                yc - rot_matrix[1][0] * length / 2) + rot_matrix[2][0] *
        (zc - rot_matrix[2][0] * length / 2)]

    bbox = bbox[
        rot_matrix[0][1] * bbox[:, 0] + rot_matrix[1][1] * bbox[:, 1] + rot_matrix[2][1] * bbox[:, 2] <
        rot_matrix[0][1] * (xc + rot_matrix[0][1] * width / 2) + rot_matrix[1][1] * (
                yc + rot_matrix[1][1] * width / 2) + rot_matrix[2][1] *
        (zc + rot_matrix[2][1] * width / 2)]

    bbox = bbox[
        rot_matrix[0][1] * bbox[:, 0] + rot_matrix[1][1] * bbox[:, 1] + rot_matrix[2][1] * bbox[:, 2] >
        rot_matrix[0][1] * (xc - rot_matrix[0][1] * width / 2) + rot_matrix[1][1] * (
                yc - rot_matrix[1][1] * width / 2) + rot_matrix[2][1] *
        (zc - rot_matrix[2][1] * width / 2)]

    bbox = bbox[
        rot_matrix[0][2] * bbox[:, 0] + rot_matrix[1][2] * bbox[:, 1] + rot_matrix[2][2] * bbox[:, 2] <
        rot_matrix[0][2] * (xc + rot_matrix[0][2] * height / 2) + rot_matrix[1][2] * (
                yc + rot_matrix[1][2] * height / 2) + rot_matrix[2][2] *
        (zc + rot_matrix[2][2] * height / 2)]

    bbox = bbox[
        rot_matrix[0][2] * bbox[:, 0] + rot_matrix[1][2] * bbox[:, 1] + rot_matrix[2][2] * bbox[:, 2] >
        rot_matrix[0][2] * (xc - rot_matrix[0][2] * height / 2) + rot_matrix[1][2] * (
                yc - rot_matrix[1][2] * height / 2) + rot_matrix[2][2] *
        (zc - rot_matrix[2][2] * height / 2)]

    return bbox


if __name__ == '__main__':

    directory_list = TRAIN_DIRECTORY
    sequence_list = TRAIN_SEQUENCES
    for directory_index in range(len(directory_list)):
        for sequence_index in range(len(sequence_list[directory_index])):

            current_directory = os.listdir(DATA_PATH + directory_list[directory_index] + '/' + sequence_list[directory_index][sequence_index])
            print(DATA_PATH + directory_list[directory_index] + '/' + sequence_list[directory_index][sequence_index])

            with open(JSON_PATH + directory_list[directory_index] + '/' + sequence_list[directory_index][sequence_index] + '/vehicle_calibration_info.json') as json_data_file:
                lidar_info = json.load(json_data_file)
                lidar_coordinates = (np.array(lidar_info['vehicle_SE3_down_lidar_']['translation']) +
                                     np.array(lidar_info['vehicle_SE3_up_lidar_']['translation'])) / 2

            for file in current_directory:
                if file.endswith('.npz'):
                    data = np.load(DATA_PATH + directory_list[directory_index] + '/' + sequence_list[directory_index][sequence_index] + '/' + file, allow_pickle=True)
                    point_cloud = data['pcl']
                    anno = data['anno']

                    point_cloud[:, 0:3] = point_cloud[:, 0:3] - lidar_coordinates

                    point_cloud = point_cloud[point_cloud[:, 2] < CROP_Z]
                    point_cloud = point_cloud[point_cloud[:, 0] < CROP_X]
                    point_cloud = point_cloud[point_cloud[:, 0] > -CROP_X]
                    point_cloud = point_cloud[point_cloud[:, 1] < CROP_Y]
                    point_cloud = point_cloud[point_cloud[:, 1] > -CROP_Y]

                    for i in range(len(anno)):
                        annotation = anno[i]
                        xc = annotation['center']['x'] - lidar_coordinates[0]
                        yc = annotation['center']['y'] - lidar_coordinates[1]
                        zc = annotation['center']['z'] - lidar_coordinates[2]

                        if annotation['label_class'] != 'VEHICLE':
                            continue
                        else:
                            bbox = cut_bounding_box(point_cloud, annotation, annotation_move=lidar_coordinates)
                            bbox = bbox[bbox[:, 4] == 1]

                        if len(bbox) > 50:
                            save_annotation = [[anno[i]['center']['x']-lidar_coordinates[0], anno[i]['center']['y']-lidar_coordinates[1], anno[i]['center']['z']]-lidar_coordinates[2], [anno[i]['rotation']['x'], anno[i]['rotation']['y'], anno[i]['rotation']['z'], anno[i]['rotation']['w']],
                                               [anno[i]['length'], anno[i]['width'], anno[i]['height']], [anno[i]['track_label_uuid'], anno[i]['timestamp'], anno[i]['label_class']]]

                            np.savez(SAVE_PATH + directory_list[directory_index] + '/' + annotation['track_label_uuid'] + '_' + str(int(np.sqrt(xc**2+yc**2))) + 'm', anno=save_annotation, pcl=bbox)
